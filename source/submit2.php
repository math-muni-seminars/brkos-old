<?php
function mail_attachment($filenames, $paths, $mailto, $from_mail, $from_name, $replyto, $subject, $message) 
{ global $page;
    $uid = md5(uniqid(time()));
	
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=utf-8\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
	
    $contents=array();
    foreach($filenames as $i=>$v)
    {
	    $content = chunk_split(base64_encode(file_get_contents($paths[$i].$v)));
	    $header .= "--".$uid."\r\n";
	    $header .= "Content-Type: application/octet-stream; name=\"".$v."\"\r\n"; // use diff. tyoes here
	    $header .= "Content-Transfer-Encoding: base64\r\n";
	    $header .= "Content-Disposition: attachment; filename=\"".$v."\"\r\n\r\n";
	    $header .= $content."\r\n\r\n";
    }
    
    $header .= "--".$uid."--";
  //  ini_set("SMTP","inserv.math.muni.cz");
//if($_SESSION["user"]->cols["nick"]=="res"){phpinfo();exit;}
   if (mail($mailto, $subject, "", $header)) {
       // echo = "mail send ... OK"; // or use booleans here
  $page->add_main("Uloženo OK.");
    } else {
$page -> add_main("Uloženo.");
	//        $page->add_main("mail send ... ERROR!");
    }
}

function is_pdf($f){
$s = strtolower($f);
return ((4+strpos($s,".pdf"))==strlen($s))||((3+strpos($s,".ps"))==strlen($s));
}

function handle_upload()
{global $page;
	$paths=$attachements=array();

	foreach($_FILES as $index=>$file)
	{
		$ser_no=$index[6];
		$task=$index[7];
		//$page->add_main("|XXX".$file["name"].preg_match("/.*\.(ps|pdf)/i",$file["name"]));
		if($file["size"] && is_pdf($file["name"]))
		{
			if($fn=searchfile("submit/serie$ser_no/task$task","soln".$_SESSION["user"]->cols["id"]."t$ser_no$task.*"))
				unlink($fn);
			//NEW	if($fn=searchfile("submit/serie$ser_no/task$task",*."_".$_SESSION["user"]->cols["id"]."${ser_no}_${task}-.*"))
			//NEW		unlink($fn);

			
			$fn="soln".$_SESSION["user"]->cols["id"]."t$ser_no$task";//NEW $fn=$_SESSION["user"]->cols["last"]."_".$_SESSION["user"]->cols["id"]."${ser_no}_${task}-";
			$fn_old=strtolower($file["name"]);//NEW $fn_old=odcestit($file["name"]);
			$allowed="abcdefghijklmnopqrstuvwxyz0123456789_.";//NEW $allowed="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.";
			for($i=0;$i<strlen($fn_old);$i++)
				if(strpos($allowed,$fn_old[$i])!==false)$fn.=$fn_old[$i];
      $dir = "submit/serie$ser_no/task$task/";
      //$dir = "files/zadani/";
      if (is_writable($dir)) 
      {
        $o=move_uploaded_file($file["tmp_name"],$dir.$fn); 
  			if($o)
  			{
  				$attachements[]=$fn;
  				$paths[]=$dir;
  				chmod($dir.$fn,0640);
			   }
        else $page->add_main("Vyskytla se chyba při odesílání.");
      } 
      else {
          $page->add_main("Zápis je dočasně zakázán.");
      }
			
		}
    elseif($file["size"]){$page->add_main("<b>Špatný typ souboru.</b>");}
	}
	if($attachements[0])
	{
		mail_attachment($attachements, $paths, "kondr@lesnimoudrost.cz", "brkos@math.muni.cz",
					"BRKOS submit", $_SESSION["user"]->cols["mail"], "Reseni $ser_no ".$_SESSION["user"]->cols["nick"], 
					$_SESSION["user"]->cols["nick"]." odeslal reseni $ser_no.serie");
	}
}


function solnlist($ser_no,$task)
{
	$dummypass=substr(md5(uniqid(time())),-8);
	$ret="<ul>";
	$dh=opendir("submit/serie$ser_no/task$task");
	$solvers=new being("id,first,last","rights>=2 order by id","");//TODO mozna
	$solved=array();
	while($fn=readdir($dh))
	{
		if($fn[0]!=".")
		{
			$solved[intval(preg_replace("/^soln(.*)t(.*)$/","$1",$fn))]=$fn;
			//NEW $solved[intval(preg_replace("/^(*)_(.*)_(.*)_(.*)-(.*)$/","$2",$fn))]=$fn;
	
			
		}
	}
	while($solvers->fetch())
	{
		if(isset($solved[$solvers->cols["id"]]))
		{
			$ret.=parse("<li>#first# #last# (
			<a href=getternot4n00bs.php?s=$ser_no&amp;t=$task&amp;u=#id#&amp;p=$dummypass>".$solved[$solvers->cols["id"]]."</a>)",$solvers);
		}
	}
	return $ret."</ul>";
}
$total = 0;
function solncount($ser_no)
{
global $total;
	$count=0;$solved=array();
for($task=1;$task<9;$task++)
{
	$dh=opendir("submit/serie$ser_no/task$task");
	while($fn=readdir($dh))
	{
		if($fn[0]!=".")
		{
$total++;
		      $uid=intval(preg_replace("/^soln(.*)t(.*)$/","$1",$fn));
			//NEW $solved[intval(preg_replace("/^(*)_(.*)_(.*)_(.*)-(.*)$/","$2",$fn))]=$fn;
			if(!isset($solved[$uid]))
		      {$count++;
			$solved[$uid]=$fn;}
			
		}
	}
}
      return $count;
}

function orgmenu($ser_no)
{
	$ret="Řešení: ";
	for($task=1;$task<9;$task++)
	{
		$c=-2;
		$dh=opendir("submit/serie$ser_no/task$task");
		while(readdir($dh))$c++;
		closedir($dh);
		$ret.="<a href=index.php?s=submit&amp;t=$ser_no&u=$task>
		$ser_no.".($task==8?"B":$task)." ($c)</a> ".($task==8?"":"|");
	}
	return $ret." Sum: ".solncount($ser_no);
}

function upload_form($ser_no)
{
	$ret="";
	$dummypass=substr(md5(uniqid(time())),-8);
	for($task=1;$task<9;$task++)
	{
		$link="<i>žádná</i>";
		if($fn=searchfile("submit/serie$ser_no/task$task","soln".$_SESSION["user"]->cols["id"]."t$ser_no$task.*"))
		//NEW if($fn=searchfile("submit/serie$ser_no/task$task","*._".$_SESSION["user"]->cols["id"]."_${ser_no}_${task}-.*"))
		{
			$date=Date("j.n.y h:i:s",filemtime($fn));
			$size=round(filesize($fn)/1024,1);
			$size=$size>1024?round($size/1024,1)." MiB":"$size KiB";
			$pieces=explode(".",$fn);
			$ext=$pieces[count($pieces)-1];
			$icon=file_exists("files/exts/$ext.gif")?"files/exts/$ext.gif":"files/exts/default.gif";
			$link="<a href=getter.php?s=$ser_no&amp;t=$task&p=$dummypass><img src=\"$icon\" alt=\"Stáhnout ($ext)\" title=\"Stáhnout ($ext)\"></a> ($date, $size)";
		}
		$ret.=("<li><strong><div>Řešení úlohy $ser_no.".($task==8?"B":$task)."</strong> Aktuální verze: $link </div>
			<label for=soubor$ser_no$task>Nahrát novou:</label> <input name=soubor$ser_no$task type=\"file\"></li>");
	}
	return $ret;
}

if($_SESSION["user"]->cols["rights"]>=2)
{

	if(isset($page->item)&&isset($page->sitem)&&$_SESSION["user"]->is_admin())
	{	
		$page->add_main(solnlist($page->item,$page->sitem));
	}
	else
	{
		$cond = $_SESSION["user"]->is_admin()?"":
		    "and zadani !='' and date >=".strtotime("-1day -1hour",time());
        //EDIT: dělá se automaticky (Zde přepsat na začátku ročníku (1993) a po Novém roce (1994))
		$serie=new serie("*","year=".(ROCNIK)." $cond","");
		if(isset($_POST["send"]))
		{
			handle_upload();
			//header("location:index.php?s=submit");
		}
		while($serie->fetch())
		{
			$ser_no=$serie->cols["no"];
			$page->add_main($serie->flush("submit"));
			if($_SESSION["user"]->is_admin())
			{
				$page->add_main(orgmenu($ser_no));
			}
if(!$_SESSION["user"]->is_admin()){
			$page->add_main("<div class=\"submitform\"><form enctype=\"multipart/form-data\" method=post action=\"index.php?s=submit\"><ul>");
			$page->add_main(upload_form($ser_no));
			$page->add_main("<input type=submit name=send value=Odeslat></ul></form></div>");}
		}
if($_SESSION["user"]->is_admin())
			{
				$page->add_main("<p>Celkem: $total</p>");
			}
	}
}
else $page->add_main("Pro využití submitovátka musíte být přihlášeni.");

?>
