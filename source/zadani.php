<?php
$TASKS = unserialize(TASKS);

if ($page->sitem < 200) $page->sitem = 141 + floor(($page->sitem - 1) / 6) * 10 + (($page->sitem - 1) % 6);
if (($page->action == "editbr_series") && ($_SESSION["user"]->is_admin())) {
    $serie = new serie("*", "year=" . floor($page->sitem / 10) . " and no=" . ($page->sitem % 10), "1");
    $page->add_main($serie->edit_form());
} elseif (!$page->item) {
//EDIT: dělá se automaticky (Zde přepsat na začátku ročníku (1993) a po Novém roce (1994))
    $serie = new serie("*", "year=" . (ROCNIK) . " order by no", "");
    $page->add_main($serie->flush_all());
    $page->add_main("<hr class=cleaner>");
    $page->add_main("<div class=\"legend\"><strong>Legenda:</strong>
<img src=\"files/images/zadani.gif\" alt=\"Zadání\"> = zadání,
<img src=\"files/images/povidani.gif\" alt=\"Pomocný text\"> = pomocný text,
<img src=\"files/images/reseni.gif\" alt=\"Řešení\"> = řešení,
<img src=\"files/images/komentar.gif\" alt=\"Komentář\"> = komentář.
.
</div>");
    if ($_SESSION["user"]->is_admin() && ($_GET["a"] == "add")) {
        $serie = new serie("", "", "");
        $page->add_main($serie->add_form($_REQUEST));
    } elseif ($_SESSION["user"]->is_admin()) {
        $nahraj = new form(array("rok" => "Ročník", "serie" => "Série", "typ" => "Typ", "soubor" => "Soubor", "send" => "Odeslat"),
            $_POST, array("soubor" => "file|.*\\.pdf", "typ" => "select|komentar|Komentář|reseni|Řešení|poradi|Výsledkovka|zadani|Zadání|povidani|Povídání", "send" => "submit"),
            array("rok", "serie", "typ"), array(), "post", "index.php?s=zadani");
        $_POST["rok"] = intval($_POST["rok"]) < 10 ? "0" . $_POST["rok"] : $_POST["rok"];
        if ($nahraj->move_uploaded("files/#typ#/#typ##rok##serie#.pdf"))
            header("Location: " . this_url(array(), 1));
        $page->add_main($nahraj->output());

        $page->add_main($_SESSION["user"]->is_admin() ?
            "\n<div><a href=\"" . this_url(array("a" => "add")) . "\">Přidat sérii</a></div>" : "");
    }
} else {
    $year = floor($page->sitem / 10);
    $no = $page->sitem % 10;
    $type = ($page->item == "zadani") ? "zadani" : ($page->item == "povidani" ? "povidani" : ($page->item == "reseni" ? "reseni" : "komentar"));
    $col = "topic,year,no" . ($type == "komentar" ? "" : ",$type");
    $serie = new serie($col, "year=$year and no=$no", "1");
    $page->add_main(strtr($serie->flush($type), array("~" => "&nbsp;")));

    if ($page->item == "komentar") {
        if ($_SESSION["user"]->is_admin()) {
            $page->add_main("Přidat komentář k úloze ");
            for ($i = 1; $i <= NTASKS; $i++) $page->add_main("<a href=\"?s=vysledky&t=" . intval($page->sitem) . "&a=koment&edit=$i\">" . $TASKS[$i - 1] . "</a>  ");
        }
        $page->add_main(output_type($page->mode));
        $problems = new problem("br_users.nick as corrector,br_problems.*,
		(select count(id) from br_results where br_results.serie=br_problems.serie and br_results.problem=br_problems.problem) as solvers,
		(select avg(points) from br_results where br_results.serie=br_problems.serie and br_results.problem=br_problems.problem) as average,
		(select sum(if(br_results.problem=br_problems.problem,points,0))/count(distinct userid) from br_results where br_results.serie=br_problems.serie) as totalaverage",
            ($_SESSION["user"]->is_admin() ? "" : "confirmed=1 and") . " br_problems.serie=" . intval($page->sitem), "");
        if ($page->mode != "TeX") {
            $page->add_main($problems->flush_all());
        } else {
            $page->add_main("<textarea rows=20 cols=50>" . $problems->flush_all("tex") . "</textarea>");
        }
    }


}
?>
