<?php
$TASKS = unserialize(TASKS);

class submit
{
    var $map = array();

    function submit($ser_no)
    {
        $ser = $ser_no % 10;
        for ($task = 1; $task <= NTASKS; $task++) {
            $this->map[$task] = array();
            $dh = opendir("submit/serie$ser/task$task");
            while ($fn = readdir($dh)) {
                if ($fn[0] != ".") {
                    //OLD   $this->map[$task][intval(preg_replace("/^soln(.*)t(.*)$/","$1",$fn))]=$fn;
                    $this->map[$task][intval(preg_replace("/^(.*)-(.*)-(.*)-(.*)$/", "$2", $fn))] = $fn;

                }
            }
        }
    }

    function check($task, $id)
    {
        return isset($this->map[$task][$id]);
    }
}

function is_floatx($str)
{
    if ($str == "") return false;
    return is_float(floatval($str));
}


function insert_results($serie, $problem)
{
    foreach ($_POST as $index => $value) {
        $value = trim(strtr($value, array("," => ".")));
        if ((substr($index, 0, 6) == "clovek") && is_floatx($value)) {
            $hodnoty[] = substr($index, 6, 10) . "," . intval($serie) . "," . intval($problem) . "," . $value;
        }
    }
    safe_query("delete from br_results where serie=" . intval($serie) . " and problem=" . intval($problem));
    if ($hodnoty) safe_query("insert into br_results values (0," . implode("), (0,", $hodnoty) . ")");
    header("Location:" . this_url(array("a" => "koment", "pg" => "detail"), 1));
}

function results_form_output($serie, $problem)
{
    $rok = YEAR + RK;
//  if(Date(m)>8) $rok=Date("Y"); 
//  else $rok=Date("Y")-1;
    $people = new being("id,first,last,city,graduate,
	                  (select points from br_results where userid=br_users.id and serie=" . intval($serie) . " and problem=" . intval($problem) . ") as points", "rights=2 order by last", "");
    $labels = $values = array();
    while ($people->fetch()) {
        if ($people->cols["graduate"] >= $rok) {
            $labels["clovek" . $people->cols["id"]] = $people->cols["first"] . " " . $people->cols["last"] . " <span style=\"font-size:12px;\">(" . $people->cols["city"] . ")</span>";
            $values["clovek" . $people->cols["id"]] = is_floatx($people->cols["points"]) ? round($people->cols["points"], 1) : "";
        }
    }
    $labels["send"] = "Odeslat";
    $form = new form($labels, $values, array("send" => "submit"));
    return "<div class=\"wideform\">" . $form->output() . "</div>";
}


function problem_headers($serie, $format, $corrected)
{
    global $TASKS;
    $numbers = range(1, NTASKS);
    if ($format) return "";
    $output = "";
    if ($_SESSION["user"]->is_admin() &&
        (my_field("br_series", "public", "concat(year,no)=$serie") != "1")) {
        foreach ($numbers as $i)
            $output .= "<th><a href=index.php?s=vysledky&t=$serie&edit=$i>" . ($TASKS[$i - 1]) . "</a><a onClick=\"sort_table(" . ($i + 3) . ")\">&darr;</a></th>";
        return $output;
    } else {
        foreach ($numbers as $i) {
            if ($corrected[$i] === true)
                $output .= "<th><a href=index.php?s=zadani&t=komentar&u=$serie#p$i>" . ($TASKS[$i - 1]) . "</a>";
            else $output .= "<th>" . ($TASKS[$i - 1]);
            $output .= "<a onClick=\"sort_table(" . ($i + 3) . ")\">&darr;</a></th>";
        }
        return $output;
    }
}

function series_headers($serie, $format)
{
    if ($format) return "";
    $ret = "";
    for ($i = 1; $i <= $serie % 10; $i++) {
        $ret .= "<th>$i<a onClick=\"sort_table(" . ($i + 3) . ")\">&darr;</a></th>";
    }
    return $ret;
}

function one_serie_table($serie, $format, $corrected)
{
    $sub = new submit($serie);
    $no = $serie % 10;
    $y = ($serie - $no) / 10;
//.vysledkovka{font-size:0.7em;} to je vse, co to znamena
    $output = $format ? "<textarea cols=50 rows=15>" :
        "<h2>Výsledky $no. série " . roman($y) . ". ročníku</h2>
		<table class=vysledkovka><thead>
	        <th><a onClick=\"sort_table(" . (NTASKS + 5) . ");\">#</a></th><th><a onClick=\"sort_table(1);\">Jméno</a></th><th><a onClick=\"sort_table(2);\">Škola</a></th><th><a onClick=\"sort_table(3);\">Ročník</a></th>";
    $output .= problem_headers($serie, $format, $corrected);
    $output .= $format ? "" : "<th><a onClick=\"sort_table(" . (NTASKS + 4) . ");\">Součet</a></th><th><a onClick=\"sort_table(" . (NTASKS + 5) . ");\">Po přepočítání</a></th></thead><tbody id=\"results\">";
    //coefficients for 09/10
    //EDIT: dělá se automaticky (na začátku roku změnit na YEAR-2, YEAR+4, na začátku ročníku na YEAR-1, YEAR+5)
    $columns = "*," . (YEAR + RK + 4) . "-graduate as year,
	" . schoolstring($format) . ", (select points from br_results where userid=br_users.id and serie=$serie limit 1) as solver";
    for ($i = 1; $i <= NTASKS; $i++) $columns .= ",(select round(points,2) from br_results where userid=br_users.id and serie=$serie and problem=$i) as p$i";
    $people = new being("$columns", "rights<4", "");
    $problem_solvers = ($problem_sum = array_fill(0, NTASKS + 2, 0));
    $solvers = 0;
    $results = array();
    while ($people->fetch()) {
        $submit = false;
        for ($i = 1; $i <= NTASKS; $i++) $submit = $submit || $sub->check($i, $people->cols["id"]);
        if ($submit || ($people->cols["solver"]) || ($people->cols["solver"] === "0")) {
            $solvers++;
            $bestsum = 0;//$people->cols["bestsum"];
            $worstP = $secondWorstP = null;
            $numberOfGoodP = 0;
            for ($i = 1; $i <= NTASKS; $i++) {
                if (($points = $people->cols["p$i"]) > 0) {
                    $people->cols["p$i"] += 0;
                    $bestsum += $points;
                    $problem_sum[$i] += $points;
                    $problem_solvers[$i]++;
                    $numberOfGoodP++;
                    if (is_null($worstP) || $worstP > $points) {
                        $secondWorstP = $worstP;
                        $worstP = $points;
                    } elseif (is_null($secondWorstP) || $secondWorstP > $points) {
                        $secondWorstP = $points;
                    }
                } elseif ($points[0] == "0") {
                    $people->cols["p$i"] = "0";
                    $problem_solvers[$i]++;
                } elseif ($sub->check($i, $people->cols["id"])) {
                    $people->cols["p$i"] = "*";
                } else {
                    $people->cols["p$i"] = $format ? "--" : ($corrected[$i] ? "&ndash;" : "?");
                }

            }

            if ($numberOfGoodP > NTAKEBEST) $bestsum -= $worstP;
            if ($numberOfGoodP > NTAKEBEST + 1) $bestsum -= $secondWorstP;

            $sum = computePointsForSeriesFromPlainSum($bestsum, computeYearOfStudyValue($people->cols["year"], $people->cols["math"] == "mat" ? 1 : 0));
            $index = round($sum * 1000000) + $people->cols["id"];
            $problem_sum[NTASKS + 2] += ($people->cols["sum"] = $sum);
            $problem_sum[NTASKS + 1] += ($people->cols["plainsum"] = $bestsum);
            $people->cols["math"] = $people->cols["math"] == "mat" ? $format ? "M" : "mat" : "";
            $results[$index] = $people->cols;
        }


    }
    $order = array_keys($results);
    rsort($order);
    foreach ($order as $foo => $bar) {
        $max_ord = $min_ord = ++$foo;
        while (isset($order[$max_ord]) && $results[$bar]["sum"] == $results[$order[$max_ord]]["sum"]) {
            $max_ord++;
        }
        while (($min_ord > 1) && ($results[$bar]["sum"] == $results[$order[$min_ord - 2]]["sum"])) {
            $min_ord--;
        }
        $ord = $max_ord == $min_ord ? "$min_ord." : ($format ? "$min_ord.--$max_ord." : "$min_ord.&ndash;$max_ord.");
        $output .= $format ? parse("$ord & #last# #first#& #year#& #schoolstring#& #math#
			&#p1|dc#&#p2|dc#&#p3|dc#&#p4|dc#&#p5|dc#&#p6|dc#&#p7|dc#&#p8|dc#&#plainsum|dc#&#sum|dc#\\\\ \\hline \n", $results[$bar]) :
            parse("<tr><th> $ord <th>#last#, #first#</th><td>#schoolstring#</td><td>#year#.#math#</td>
		               <td>#p1|dc#</td><td>#p2|dc#</td><td>#p3|dc#</td><td>#p4|dc#</td>
			       <td>#p5|dc#</td><td>#p6|dc#</td><td>#p7|dc#</td><td>#p8|dc#</td><td>#plainsum|dc#</td>
			       <td>#sum|dc#</td></tr>\n", $results[$bar]);
    }
    if (!$format) {
        $output .= "</tbody><tfoot><tr><th colspan=4>Průměr</th>";
        for ($i = 1; $i <= NTASKS; $i++) $output .= $problem_solvers[$i] ? "<td><small>" . round($problem_sum[$i] / $problem_solvers[$i], 2) .
            "<br>(" . round($problem_sum[$i] / $solvers, 2) . ")</small></td>" : "<td><small>?</small></td>";
        for ($i = NTASKS + 1; $i < NTASKS + 3; $i++) $output .= $solvers ? "<td><small>" . round($problem_sum[$i] / $solvers, 2) . "</small></td>" : "<td><small>?</small></td>";
    }
    $output .= $format ?
        "</textarea>" : "</tfoot></table>";
    return $output;
}

function complete_table($serie, $format)
{
    $no = $serie % 10;
    $y = ($serie - $no) / 10;
    $output = $format ? "<textarea  cols=50 rows=15>" :
        "<h2>Průběžné pořadí po $no. sérii " . roman($y) . " ročníku</h2>
	        <table class=vysledkovka><thead>
	        <tr><th><a onClick=\"sort_table(" . ($no + 4) . ");\">#</a></th><th><a onClick=\"sort_table(1);\">Jméno</a></th><th><a onClick=\"sort_table(2);\">Škola</a></th><th><a onClick=\"sort_table(3);\">Ročník</a></th>";
    $output .= series_headers($serie, $format);
    $output .= $format ? "" : "<th><a onClick=\"sort_table(" . ($no + 4) . ");\">Součet</a></th></thead><tbody id=\"results\">";
    //EDIT: dělá se automaticky (na začátku roku změnit na YEAR-2, YEAR+4, na začátku ročníku na YEAR-1, YEAR+5)
    $columns = "*," . (YEAR + RK + 4) . "-graduate as year,
	" . schoolstring($format) . ",
        (select points from br_results where userid=br_users.id and serie>" . ($serie - $no) . " limit 1) as solver";
    for ($i = floor($serie / 10) * 10 + 1; $i <= $serie; $i++) {
        for ($j = 1; $j <= NTASKS; $j++) $columns .= ",(select round(points,2) from br_results where userid=br_users.id and serie=$i and problem=$j) as s".($i % 10)."p".$j;
        $tablecols .= $format ? "&#p" . ($i % 10) . "|dc#" :
            "<td>#p" . ($i % 10) . "|dc#<br>#s" . ($i % 10) . "|dc#</td>";
    }
    $people = new being("$columns", "rights<4", "");
    //$output.=$columns;
    while ($people->fetch()) {
        if (($people->cols["solver"]) || ($people->cols["solver"] === "0")) {
            $plainsum = $sum = 0;
            for ($i = 1; $i <= $no; $i++) {
                $bestsum = 0;
                $worstP = $secondWorstP = null;
                $numberOfGoodP = 0;
                $numberOfBadP = 0;
                for ($j = 1; $j <= NTASKS; $j++) {
                    if (($points = $people->cols["s" . $i . "p" . $j]) > 0) {
                        $people->cols["s" . $i . "p" . $j] += 0;
                        $bestsum += $points;

                        $numberOfGoodP++;
                        if (is_null($worstP) || $worstP > $points) {
                            $secondWorstP = $worstP;
                            $worstP = $points;
                        } elseif (is_null($secondWorstP) || $secondWorstP > $points) {
                            $secondWorstP = $points;
                        }
                    } elseif ($points[0] == "0") {
                        $numberOfBadP++;
                    }
                }

                if ($numberOfGoodP > NTAKEBEST) $bestsum -= $worstP;
                if ($numberOfGoodP > NTAKEBEST + 1) $bestsum -= $secondWorstP;

                if ($numberOfGoodP + $numberOfBadP > 0) {
                    $people->cols["p$i"] = computePointsForSeriesFromPlainSum($bestsum, computeYearOfStudyValue($people->cols["year"], $people->cols["math"] == "mat" ? 1 : 0));
                    $sum += $people->cols["p$i"];
                    $plainsum += $bestsum;//$people->cols["s$i"];
                } else $people->cols["p$i"] = ($people->cols["p$i"][0] == "0" ? "0" : ($format ? "--" : "&ndash;"));
            }

            $people->cols["math"] = $people->cols["math"] == "mat" ? $format ? "M" : "mat" : "";
            $index = round($sum * 1000000) + $people->cols["id"];
            $people->cols["sum"] = $sum;
            $people->cols["partsum"] = $sum - (is_floatx($people->cols["p$no"]) ? $people->cols["p$no"] : 0);
            $people->cols["plainsum"] = $plainsum;
            $results[$index] = $people->cols;
        }
    }
    $order = array_keys($results);
    rsort($order);
    foreach ($order as $foo => $bar) {
        $output .= $format ?
            parse(($foo + 1) . ".& #last# #first#& #year#.& #schoolstring#& #math#
		$tablecols &#sum|dc#\\\\ \\hline\n", $results[$bar]) :
            parse("<tr><th>" . ($foo + 1) . ".<th>#last#, #first#</th><td>#schoolstring#<td>#year#.#math#
		               $tablecols<td>#sum|dc#<br />#plainsum|dc#</td></tr>\n", $results[$bar]);
    }
    $output .= $format ? "</textarea>" : "</tbody></table>";

    return $output;
}

function computeYearOfStudy($graduate){
    return YEAR + RK + 4 - $graduate;
}

function computeYearOfStudyValue($year, $isMath){
    return $year + $isMath;
}

function computePointsForSeries(array $results, $year_of_study_value){
    return computePointsForSeriesFromPlainSum(array_sum($results), $year_of_study_value);
}

function computePointsForSeriesFromPlainSum($plainSum, $year_of_study_value){
    $sum = $plainSum;
    $coef = (6.0 - $year_of_study_value) / 12.0;

    if ($plainSum <= 12) {
        $sum += $plainSum * $coef;
    } else {
        $sum += (24.0 - $plainSum) * $coef;
    }
    
    return round($sum, 2);
}

function navigation($minserie, $maxserie)
{
    $rocnik = ($minserie - 1) / 10;
    $ret = "";
    for ($i = $minserie; $i <= $maxserie; $i++)
        $ret .= "<div class=col> <a href=\"" . this_url(array("t" => $i, "pg" => "NOT_SET")) . "\">Po " . ($i - 10 * $rocnik) . " sérii</a>
	   <br>
	   <a href=\"" . this_url(array("t" => $i, "pg" => "detail")) . "\">" . ($i - 10 * $rocnik) . " série</a>
	   </div>";
    return $ret;
}

function addr_labels($minserie, $maxserie)
{
    $result = safe_query("select distinct(userid) from br_results where serie>=$minserie and serie<=$maxserie");
    $ids = "(";
    while ($row = mysql_fetch_row($result))
        $ids .= $row[0] . ",";
    $ids[strlen($ids) - 1] = ")";//replace , by )
    $ppl = new being("*,(select concat(fullname,'\\\\\\\\',street,'\\\\\\\\',postalcode,' ',city) from br_schools where br_schools.id=schoolid) as schooladdress", "id in $ids  order by last", "");
    $ret = "<textarea rows=20 cols=70>\documentclass[a4paper,12pt]{letter}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[noprintbarcodes,nocapaddress]{envlab}
\SetLabel{70mm}{44.8mm}{8mm}{1cm}{10mm}{3}{7}
\makelabels
\begin{document}
\startlabels \n" .
        $ppl->flush_all("label") . "\end{document}</textarea>";
    return $ret;
}

if ($_SESSION["user"]->is_admin() && $page->edit) {
    if ($_GET["a"] == "koment") {
        $problem = new problem("*", "br_problems.serie=" . intval($_GET["t"]) . " and br_problems.problem=" . intval($_GET["edit"]), "");
        if ($problem->fetch()) {
            $page->add_main($problem->edit_form($_POST));
        } else {
            $page->add_main($problem->add_form($_POST));
        }
    } else {
        $page->add_main("<a href=" . this_url(array("a" => "NOT_SET", "edit" => "NOT_SET", "pg" => "detail")) . ">Zpět na výsledkovku</a>");
        if (isset($_POST["send"])) {
            insert_results($page->item, $page->edit);
        } else
            $page->add_main(results_form_output($page->item, $page->edit));
    }
} else {
    $page->add_main(output_type($page->mode));//tex/html switch link

    $cond = $_SESSION["user"]->is_admin() ? "1" : "public=1";
    $maxserie = my_field("br_series", "concat(year,no)", "$cond order by year desc, no desc");
    $rocnik = floor($maxserie / 10);
    $minserie = 10 * $rocnik + 1;

    $page->add_main(navigation($minserie, $maxserie) . "<hr class=\"cleaner\">");

    $serie = ($page->item && ($page->item <= $maxserie) && ($page->item >= $minserie)) ? $page->item : $maxserie;

    //$cond=($page->sitem?"serie=$serie":"serie>$rocnik"."0 and serie<=$serie");


    if ($page->sitem) {
        $corrected = array();
        $problems = new problem("br_problems.problem", "br_problems.confirmed=1 and br_problems.serie=$serie", "");
        while ($problems->fetch()) {
            $corrected[$problems->cols["problem"]] = true;
        }
        if ($page->mode != "addr") {
            $page->add_main(one_serie_table($serie, $page->mode, $corrected));
        }
        else {
            $page->add_main(addr_labels(max($serie - 2, 1), $serie));
        }
    } else {
        if ($page->mode != "addr") {
            $page->add_main(complete_table($serie, $page->mode));
        }
        else {
            $page->add_main(addr_labels($minserie, $maxserie));
        }

    }

}

?>

