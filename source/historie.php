<?php
$page->add_main("<h2>Archiv</h2>\n<table>
	<th>Série</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>");

//EDIT: dělá se automaticky (Změnit na začátku ročníku na 1994, po Novém roce na 1995)
for($rocnik=ROCNIK-1;$rocnik>0;$rocnik--)
{
	$page->add_main("<tr><th rowspan=2>Ročník ".roman($rocnik)."</th>");
	$rocnikt=$rocnik<10?"0$rocnik":$rocnik;
	
	//Tom pridano 1. 3. 2016
	for($serie=1;$serie<7;$serie++)	
	{
		$page->add_main("<td valign=top style=\"border-bottom-color:transparent\">\n");
		
		$nazevSerie=new serie("topic","year=$rocnik and no=$serie","1");
		$page->add_main("".$nazevSerie->cols["topic"]."");
		$page->add_main("</td>");
	}
	$page->add_main("</tr>");
	$page->add_main("<tr>");
	for($serie=1;$serie<7;$serie++)	
	{
		$page->add_main("<td valign=top>\n");
		
		foreach(array("zadani"=>"Zadání","reseni"=>"Řešení","povidani"=>"Povídání","komentar"=>"Komentář","poradi"=>"Pořadí") as $typ=>$alt)
		if(file_exists($fn="files/$typ/$typ$rocnikt$serie.pdf"))
			$page->add_main("<a href=\"$fn\"><img src=\"files/images/$typ.gif\" alt=\"$alt\" title=\"$alt\"></a>\n");
		$page->add_main("</td>\n");
	}
	$page->add_main("</tr>");
}
$page->add_main("</table>
	<div class=\"legend\">
	<strong>Legenda:</strong>
<img src=\"files/images/zadani.gif\" alt=\"Zadání\"> = zadání,
<img src=\"files/images/reseni.gif\" alt=\"Řešení\"> = řešení,
<img src=\"files/images/poradi.gif\" alt=\"Pořadí\"> = pořadí,
<img src=\"files/images/komentar.gif\" alt=\"Komentář\"> = komentář,<br />
<img src=\"files/images/povidani.gif\" alt=\"Pomocný text\"> = pomocný text.
</div>");
?>
