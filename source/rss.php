<?php

class RSS
//{{{
# trida pro generovani RSS-kanalu
# verze 0.1
# zavislosti: zadne
{
    # vlastnosti
    //{{{
    var $description;         # popis RSS kanalu
    var $pub_date;            # datum publikovani obsahu kanalu
    var $ttl;                 # Time-To-Live cache kanalu
    var $image = array();     # obrazek prislusny ke kanalu; asociativni 
                                    #   pole o polozkach:
                                    #   [url]
                                    #   [title]
                                    #   [link]
                                    #   [width]
                                    #   [height]
                                    #   [description]
    var $stylesheet;          # CSS-stylesheet RSS-feedu
    var $items = array();     # pole polozek kanalu (typu RSS_Item)
    //}}}
    
    # metody rozhrani
    //{{{
    
    function set_description($description)
    //{{{
    # void set_description (string description)
    # nastavi popis kanalu na $description
    {
        $this->description = $description;
    }
    //}}}
    
    function set_pub_date($pub_date)
    //{{{
    # void set_pub_date (string pub_date)
    # nastavi datum publikovani obsahu kanalu na $pub_date
    # ocekava $pub_date ve formatu RFC822
    {
        $this->pub_date = $pub_date;
    }
    //}}}
    
    function set_ttl($ttl)
    //{{{
    # void set_ttl (int ttl)
    # nastavi Time-To-Live pro cache kanalu
    # ocekava cele cislo - pocet minut
    {
        $this->ttl = $ttl;
    }
    //}}}
    
    function set_image($url,$title,$link=ABSOLUTE_ROOT_URI,$width=null,$height=null,$description=null)
    //{{{
    # void set_image (string url, string title [, string link [, int width [, int height [, string description]]]])
    # nastavi pro kanal obrazek adresy $url alternativni popiskou $title 
    #   odkazujici na adresu $link (implicitne adresa korene systemu), o sirce
    #   $width (implicitne 88, maximalne 144) a vysce $height (implicitne 31, 
    #   maximalne 400) a titulku $description
    {
        $this->image = array(
            'url'           => $url,
            'title'         => $title,
            'link'          => $link,
            'width'         => $width,
            'height'        => $height,
            'description'   => $description
        );
    }
    //}}}
    
    function set_stylesheet($url)
    //{{{
    # void set_stylesheet (string url)
    # nastavi XML-stylesheet RSS-feedu na $url
    {
        $this->stylesheet = $url;
    }
    //}}}
    
    function add_item($item)
    //{{{
    # void add_item (RSS_Item item)
    # prida do kanalu polozku $item
    {
        $this->items[] = $item;
    }
    //}}}
    
    function generate()
    //{{{
    # string generate ()
    # vygeneruje RSS kanal podle nastavenych udaju
    # vrati vygenerovany XML dokument obsahujici RSS kanal
    {
        # pro jednodussi zapis vystup pres buffer
        ob_start();
        echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
        if (!empty($this->stylesheet))
        {
            echo '<?xml-stylesheet type="text/css" href="' . $this->stylesheet . '"?>' . "\n";
        }
?>
        <rss version="2.0">
          <channel>
            <title><?php echo WEB_NAME; ?></title>
            <link><?php echo ABSOLUTE_ROOT_URI; ?></link>
            <description><?php echo $this->description; ?></description>
            <language>cs</language>
            <webMaster><?php echo EMAIL_ADMIN; ?></webMaster>
<?php
        if (!empty($this->pub_date))
        {
?>
            <pubDate><?php echo $this->pub_date; ?></pubDate>
<?php
        }
?>
            <lastBuildDate><?php echo date('r'); ?></lastBuildDate>
            <docs>http://blogs.law.harvard.edu/tech/rss</docs>
<?php
        if (!empty($this->ttl))
        {
?>
            <ttl><?php echo $this->ttl; ?></ttl>
<?php
        }
        
        if (!empty($this->image))
        {
?>
            <image>
              <url><?php    echo $this->image['url'];    ?></url>
              <title><?php  echo $this->image['title'];  ?></title>
              <link><?php   echo $this->image['link'];   ?></link>
<?php
            if (!is_null($this->image['width']))
            {
?>
              <width><?php  echo $this->image['width'];  ?></width>
<?php
                
            }

            if (!is_null($this->image['height']))
            {
?>
              <width><?php  echo $this->image['height'];  ?></width>
<?php
                
            }

            if (!is_null($this->image['description']))
            {
?>
              <description><?php echo $this->image['description']; ?></description>
<?php
            }
?>
            </image>
<?php
        }
        
        foreach ($this->items as $item)
        {
            $item->show();
        }
?>
          </channel>
        </rss>
<?php
        # vypis obsahu z bufferu
        $rss_contents = ob_get_contents();
        # vyprazdneni bufferu
        ob_end_clean();
        # vraceni obsahu
        return $rss_contents;
    }
    //}}}
  
    //}}}
}
//}}}


class RSS_Item
//{{{
# trida pro generovani polozky RSS-kanalu
# verze 0.1
# zavislosti: zadne
{
    # vlastnosti
    //{{{
    var $title;              # titulek polozky
    var $link;               # URL odkazujici na polozku
    var $description;        # strucny prehled o polozce (kratky vytah)
    var $author;             # e-mail na autora prispevku
    var $guid;               # identifikator polozky
    var $pub_date;           # cas zverejneni polozky
    var $category;           # kategorie
    
    //}}}
    
    # specialni metody
    //{{{
    
    function RSS_Item($title=NULL,$link=NULL,$description=NULL)
    //{{{
    # RSS_Item __construct ([string title [, string link [, string description]]])
    # konstruktor
    # pokud je dany parametr zadan nastavi titulek polozky na $title, URL na 
    #   polozku $link, popis polozky $description
    {
        $this->title        = (is_null($title)       ? NULL : $title);
        $this->link         = (is_null($link)        ? NULL : $link);
        $this->description  = (is_null($description) ? NULL : $description);
    }
    //}}}
    
    //}}}
    
    # metody rozhrani
    //{{{
    
    function set_title($title)
    //{{{
    # void set_title (string title)
    # nastavi titulek polozky na $title
    {
        $this->title = $title;
    }
    //}}}
    
    function set_link($link)
    //{{{
    # void set_link (string link)
    # nastavi URL odkaz polozky na $link
    {
        $this->link = $link;
    }
    //}}}
    
    function set_description($description)
    //{{{
    # void set_description (string description)
    # nastavi strucny popis polozky na $description
    {
        $this->description = $description;
    }
    //}}}
    
    function set_author($author)
    //{{{
    # void set_author (string author)
    # nastavi e-mail autora polozky na $author
    {
        $this->author = $author;
    }
    //}}}
    
    function set_guid($guid)
    //{{{
    # void set_guid (string guid)
    # nastavi identifikator polozky na $guid
    {
        $this->guid = $guid;
    }
    //}}}
    
    function set_pub_date($pub_date)
    //{{{
    # void set_pub_date (string pub_date)
    # nastavi cas zverejneni polozky na $pub_date (v RFC 822 formatu)
    {
        $this->pub_date = $pub_date;
    }
    //}}}
    function set_category($category)
    //{{{
    # void set_pub_date (string pub_date)
    # nastavi cas zverejneni polozky na $pub_date (v RFC 822 formatu)
    {
        $this->category = $category;
    }
    //}}}
    
    function show()
    //{{{
    # void show ()
    # vytiskne RSS polozku
    {
?>
        <item>
<?php
        if (!is_null($this->title))
        {
?>
          <title><?php echo $this->title; ?></title>
<?php
        }
        
        if (!is_null($this->link))
        {
?>
          <link><?php echo $this->link; ?></link>
<?php
        }
        
        if (!is_null($this->description))
        {
?>
          <description><?php echo $this->description; ?></description>
<?php
        }
        if (!is_null($this->category))
        {
?>
          <category><?php echo $this->category; ?></category>
<?php
        }
        
        if (!is_null($this->author))
        {
?>
          <author><?php echo $this->author; ?></author>
<?php
        }
        
        if (!is_null($this->guid))
        {
?>
          <guid isPermaLink="false"><?php echo $this->guid; ?></guid>
<?php
        }
        
        if (!is_null($this->pub_date))
        {
?>
          <pubDate><?php echo $this->pub_date; ?></pubDate>
<?php
        }
?>
        </item>
<?php
    }
    //}}}
    
    //}}}

}
//}}}
?>
