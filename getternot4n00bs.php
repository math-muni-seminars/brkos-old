<?php
define("TEXY_ADDR", "texy/texy.php");
$ser_no = intval($_GET["se"]);
$task = intval($_GET["t"]);
$finfo = finfo_open(FILEINFO_MIME_TYPE);
if ($_SESSION["user"]->is_admin()
    //OLD &&($fn=searchfile("submit/serie$ser_no/task$task","soln".intval($_GET["u"])."t$ser_no$task.*"))
    && ($fn = glob("./submit/serie$ser_no/task$task/*-" . intval($_GET["u"]) . "-t$ser_no$task-*"))
) {
    header('Cache-control: private');
    header('Content-Length: '.filesize($fn[0]));
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf');
    header('Content-Disposition: inline; filename='.basename($fn[0]));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    ob_clean();

    flush();

    readfile($fn[0]);

} else header("location: index.php?s=submit&t=$ser_no-$task-" . intval($_GET["u"]));
?>
