<?php
require "source/rss.php";
require "config/config.php";
require "source/class.php";
Define("WEB_NAME","BRKOS :: novinky");
Define("ABSOLUTE_ROOT_URI","http://math.muni.cz/~brkos/");
Define("RSS_DESCRIPTION","Aktuální dění na webu brněnského korespondenčního semináře.");
Define("EMAIL_ADMIN","kondr@lesnimoudrost.cz");


dbconnect();
Header("Content-Type: application/rss+xml; encoding=utf-8");

$Prispevky=@mysql_query("select * from br_news order by id desc limit 30");
$RSS = new RSS;
$RSS->set_description(RSS_DESCRIPTION);
while ($Prispevek=@mysql_fetch_assoc($Prispevky)) {
	$Item= new RSS_Item;
	$Item->set_title(strtr($Prispevek["name"],array("&"=>"&amp;")));
	$Item->set_guid("http://bart.math.muni.cz/~brkos/index.php?s=news#p".$Prispevek["id"]);
	$Item->set_link("http://bart.math.muni.cz/~brkos/index.php?s=news#p".$Prispevek["id"]);
	$content=strtr($Prispevek["content"],array("&"=>"&amp;","<"=>"&lt;",">"=>"&gt;"));
	$Item->set_description(preg_replace("/norss(.*)ssron/s","",$content));
	$Item->set_category("Novinky");
	$Item->set_author("Brkos Team");
	$Item->set_pub_date(Date("r",$Prispevek["date"]));
	$Items[]=$Item;
}
foreach ($Items as $Item)
{
	$RSS->add_item($Item);
}
echo $RSS->generate();

?>

