<?php
//ini_set("session.use_only_cookies",1);
//ini_set("session.use_trans_sid",0);
ini_set("display_errors","E_NONE");
//error_reporting("E_ALL");
require"config/config.php";
require "source/class.php";
session_start();
$page=new page();

if(dbconnect())
{

	if(!isset($_SESSION["sciuser"])||!is_object($_SESSION["sciuser"]))
	{ 
		//session_register("message");
		//session_register("sciuser");
		$_SESSION["sciuser"]=new being("","","");
	}

	$_SESSION["sciuser"]->handle_login();	

/* OLD TIMESTAMP
if($_SESSION["sciuser"]->cols["rights"]>2){define("START",1340114400-6*3600);}
else{define("START",1340114400);}
define("START_REAL",1340114400);
*/
/*
mktime(16, 0, 0, 11, 11, 2015);
// mktime(hodiny, minuty, sekundy, mesic, den, rok)
//11. 11. 2015 16:00 je 1447254000
// 9. 11. 2016 16:00 je 1478703600
*/

  if($_SESSION["sciuser"]->is_tester()){
    define("START", mktime(20, 0, 0, 11, 30, 2021));
  } else {
    define("START", mktime(16, 0, 0, 12, 1, 2021));
  }

  define("START_REAL", mktime(16, 0, 0, 12, 1, 2021));

	if($page->site)$page->add_fixed(mysql_escape_string($page->site));
	if(($page->status!="404") && file_exists($fn="source/".$page->site.".php")){include $fn;}

  $query = array();
  foreach ($_GET as $key => $value) {
    $query[$key] = htmlspecialchars($value);
  }

	$search=new form(array("phrase"=>"Hledat","search"=>"OK","s"=>"","topic"=>""),$query,
	                 array("s"=>"hidden","topic"=>"hidden","search"=>"submit"),
	                 array(),array(),"get");
	$page->add_div("search","<div id=\"searchform\">".$search->output()."</div>");
	$page->add_div("login",$_SESSION["sciuser"]->loginner());
	if($_SESSION["sciuser"]->is_admin())$page->add_script("confirm.js");
}
else
{
        $_SESSION["sciuser"]=new being("","","");
        $page->add_main("<h1>Chyba serveru</h1>Nepodařilo se navázat spojení s databází. Možná se jedná o dočasný výpadek, 		zkuste to prosím pzději. Díky za pochopení.");
        $page->add_title("chyba databáze");
        $page->add_div("login","Nelze se přihlásit.");
}

if(strpos($page->main,"thickbox"))
{
	$page->add_script("jquery-latest.pack.js");
	$page->add_script("thickbox.js");
	$page->add_div("style",'<link rel="stylesheet" href="files/css/thickbox.css" type="text/css" media="screen" />');
}
else $page->add_div("style","");
if(!$page->redirect)
{	
	$page->set_template('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>


  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="robots" content="index,follow">
  <meta name="googlebot" content="index,follow,snippet,noarchive">
  <meta name="author" content="Kondr (kondr@lesnimoudrost.cz)">
  <meta name="description" content="#description# MathRace  je matematická soutěž Ústavu matematiky a statistiky PřF MU. Její první ročník 2.12.2009 pořádá BRKOS Team.">
  <title>#title# | MathRace &ndash; Matematická soutěž Přirodovědecké fakulty MU</title>
  <link href="index_files/mathstyle.css" rel="stylesheet" type="text/css">
  <link href="index_files/thickbox.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" href="http://math.muni.cz/favicon.ico">
  #scripts#
  <script type="text/javascript" src="files/cas.js"></script>
</head>
<body onload="cas()">
<div id="page">
 <div id="ban"><h1><a href="./">MathRace -- matematická soutěž PřF MU</a></h1>
<a href="./"><span></span></a></div>
 <!-- MENU --> 
   	<ul id="navlist">
		<li><a href="?s=pravidla" '.($page->site=="pravidla"?"id=\"current\"":"").'>Pravidla</a></li>
		<li><a href="?s=o_soutezi" '.($page->site=="o_soutezi"?"id=\"current\"":"").'>O soutěži</a></li>
		<li><a href="?s=tymy" '.($page->site=="tymy"?"id=\"current\"":"").'>Týmy</a></li>'
    . ($_SESSION["sciuser"]->is_logged()?''
      .'<li><a href="?s=nastaveni" '.($page->site=="nastaveni"?"id=\"current\"":"").'>Náš tým</a></li>'
      . (Time() >= START ? '<li><a href="?s=answers" '.($page->site=="answers"?"id=\"current\"":"").'>Odpovědět</a></li>' : "") 
      : '<li><a href="?s=registrace" '.($page->site=="registrace"?" id=\"current\"":"").'>Registrace</a></li> '
    )

    . (Time() >= START ? 
      '<li><a href="?s=zadani" '.($page->site=="zadani"?"id=\"current\"":"").'>Zadání</a></li> '.
      '<li><a href="?s=reseni" '.($page->site=="reseni"?"id=\"current\"":"").'>Řešení</a></li> '.
      '<li><a href="?s=vysledky" '.($page->site=="vysledky"?"id=\"current\"":"").'>Výsledky</a></li> '.
      '<li><a href="?s=stats" '.($page->site=="stats"?"id=\"current\"":"").'>Statistiky</a></li> '
    : '')
    . '<li><a href="?s=diskuse" '.($page->site=="diskuse"?"id=\"current\"":"").'>Diskuse</a></li>
		<li><a href="?s=orgs" '.($page->site=="orgs"?"id=\"current\"":"").'>Organizátoři</a></li>
    <li><a href="?s=archiv" '.($page->site=="archiv"?"id=\"current\"":"?s=archiv").'>Archiv</a></li>'
    . ($_SESSION["sciuser"]->is_admin() ? '<li><a href="?s=fill_answers" '.($page->site=="fill_answers"?"id=\"current\"":"").'>Zadat odpovědi</a></li>' : "")
	. '</ul>
  <!-- /MENU -->
<!-- OBSAH -->
 <div class="obsah">
      <div class="drobky"><span>Navigace: '.($page->title?"<a href=index.php>MathRace</a> &gt;".$page->title:"Vítejte na stránkách MathRace!").'</span></div>'.
/* <strong>Děkujeme za hru a prosíme tři vítězné týmy, aby si vybrali ceny prostřednictvím diskuse.</strong>. */
'      <div class="panel">
      #login#
        <h2>Čas serveru</h2>
	<div id=cas>'.Date("H:i:s").'</div>
      </div>
<div class="telo">
	'.$_SESSION["message"].'
		#main#
</div>

 </div>   
<!-- /OBSAH --> 
<!-- PATICKA -->
<br style="clear: both;">
  <div class="paticka">
<div style="text-align:center">
<div style="margin: 10px;">
<img src="/files/images/jcmf.jpg" height="100"/>
<img src="/files/images/muni-math.png" height="100"/>
</div>
<br />
Popularizace vědy a výzkumu v přírodních vědách a matematice s využitím potenciálu a finanční podporou Ústavu matematiky a statistiky Masarykovy univerzity, České matematické společnosti a Ministerstva školství, mládeže a tělovýchovy.
</div>
<div>
Design &#169; 2006 <!--<a href="mailto:ivuska.h@gmail.com">-->Ivana Halabalová&nbsp;</div>
  </div>
'."<script type=\"text/javascript\">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16276547-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>".'
<!-- /PATICKA --> 
</div>
</body></html>');
echo $page->flush();
$_SESSION["message"]="";
}
?>
