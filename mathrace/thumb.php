<?php
# THUMBNAIL SCRIPT
# 26 September 2004
# by Ondrej Bouda [obouda@kompl.cz]
# http://ripper.iglu.cz/scripts/thumb/


# DESCRIPTION OF INPUT:
# file:     required, string; filename or URL address of the image
# type:     optional, string "jpeg"/"png"/"wbmp"; output type of the image; by default it uses the original type
# quality:  optional, integer 0-100; if output file type is jpeg, it uses this quality; default value is 75
# width:    optional, integer; desired (maximum) width of the image
# height:   optional, integer; desired (maximum) height of the image
# ratio:    optional, bool; specifies, whether preserve aspect ratio or not, if true, then width/height will be used as the biggest width/height; default value is true
# increase: optional, float; if neither height nor width is specified, the result image will be $increase-times larger
# decrease: optional, float; if neither height nor width is specified, the result image will be $decrease-times smaller
# nocache:  optional, ; if specified, cache won't be used
# nocheck:  optional, ; if specified, size of the cached image won't be checked while deciding whether use cached image or generate new
# enlarge:  optional, ; if specified, even small images will be enlarged to desired size (so by default they won't)
# subdir:   optional, ; if specified, $file's path is a subdirectory of $path, so the beginning (the same part as $path) of the $file is taken away

error_reporting(1);


# you can change the following:
	$defaultQuality = 75;   # integer 0-100; default jpeg quality
	$defaultWidth   = THUMB_SIZE;# integer; if nothing of width,height,increase,decrease is specified, this width will be used if not null
	$defaultHeight  = THUMB_SIZE;# integer; if nothing of width,height,increase,decrease is specified, this height will be used if not null
	$path = "";     # string; path to the images
        $path_cache = "cache"; # string; path to the cache
	$wbmpSupport = true;    # bool; specifies whether the server supports wbmp output, if not, jpeg will be used instead
	$use_db = false;        # bool; specifies whether use database selection or passed file
    $use_cache = true;      # bool; specifies whether use cache (cache directory specified in $path_cache); cached images are only these which use default image size and type
    $allow_cache_dir_level = 1; # integer; specifies what is the maximum directory level for cached images

	# connection to database
	$db_host = "localhost:3306"; # string; hostname
	$db_user = "";               # string; username
	$db_pass = "";               # string; password
	$db_table = "images";        # string; name of the table with images
	$db_field = "file";          # string; name of the field with image address
	$db_parameter = "id";        # string; parameter passed in URL for comparison




extract($_REQUEST,EXTR_SKIP);

# default values:
	if (!isset($defaultQuality)) $defaultQuality = 75;
	if (!isset($ratio)) $ratio = true;
	if (!isset($quality)) $quality = $defaultQuality;
	if (!isset($decrease)) {
        $decrease = 1;
        $defaultDecreaseUsed = true;
    } else
        $defaultDecreaseUsed = false;

# default variable types:
	settype($ratio,"bool");
	settype($quality,"integer"); if ($quality < 0 || $quality > 100) $quality = $defaultQuality;
	settype($path,"string");
	settype($defaultWidth,"integer");
	settype($defaultHeight,"integer");
	if (isset($file)) settype($file,"string");
	if (isset($width)) settype($width,"integer");
	if (isset($height)) settype($height,"integer");
	if (isset($increase)) settype($increase,"float");
	if (isset($decrease)) settype($decrease,"float");

# variables for internal use
	$error = "";
	$new_width = 0;
	$new_height = 0;
	$image_types = array(
        1=>"gif",
        2=>"jpeg",
        3=>"png",
        6=>"bmp",
        15=>"wbmp",
        16=>"xbm"
    );
    define('ABSOLUTE_URI', ABSOLUTE_ROOT_URI); # an attempt to auto-generate this would be nice
# if $subdir specified, change $file's beginning
    if (isset($_GET['subdir']))
    {
        if (strpos($file,$path)===0)
        {
            $file = substr($file,strlen($path));   
        }
    }
    
# this is the place for use database selection if needed, this way you can pass just id or something in URL:
	if ($use_db) {
		if (!mysql_connect($db_host,$db_user,$db_pass))
			$error.= "Error connecting to database.\n";
		else {
			if (!$result = mysql_query("SELECT $db_field FROM $db_table WHERE $db_parameter=$$db_parameter"))
				$error.= "Database query error.\n";
			else {
				if (!$result = mysql_result($result,0))
					$error.= "Database error.\n";
				else
					$file = $result;
			}
		}
	}
# copy filename before modification
    $filename = $file;
# if file isn't an URL, specified path to the images will be used
	if ($path && strpos($file,"://")===false) $file = $path.$file;
# check whether everything's ok
	if (!$file)
		$error.= "File not specified.\n";
    if (!$error && !file_exists($file))
		$error.= $file;
# gather properties of the given image
    list($original_width,$original_height,$original_type) = getimagesize($file);
# supported image types for writing are: jpeg, png, wbmp, gif
	if ($type) {
		if ($type!="jpeg" && $type!="png" && $type!="wbmp" && $type!='gif')
			$error.= "Unsupported output type.\n";
        $defaultTypeUsed = false;
	} else {
    # if output type isn't specified, use original type of the opened image
		$type = $image_types[$original_type];
    # if type of the opened image is gif/xbm, use png/jpeg instead
		switch ($type) {
			case "xbm":
				$type = "jpeg";
				break;
		}
		if ($image_types[$original_type]=="wbmp" && $wbmpSupport==false)
			$type = "jpeg";
        $defaultTypeUsed = true;
	}
# if cache enabled, check if the result exists and if it's the same as the result would be, and if so, redirect instead of generating
    if ($use_cache && !isset($_GET['nocache']) && !isset($_GET['enlarge']) && $defaultTypeUsed && 
        file_exists($path_cache . $filename))
    {
        # if size checking disabled, redirect immediately
        if (isset($_GET['nocheck']))
        {
            header('Location: ' . ABSOLUTE_URI . $path_cache . $filename);
            exit;
        }
        # check size of the image
        list($image_size_width,$image_size_height) = getimagesize($path_cache . $filename);
        $image_ratio = $image_size_width/$image_size_height; 
        if (($image_ratio<1 && round($image_size_width/$image_ratio)==$defaultWidth) || 
            ($image_ratio>=1 && round($image_size_height*$image_ratio)==$defaultHeight))
        {
            header('Location: ' . ABSOLUTE_URI . $path_cache . $filename);
            exit;
        }
    }
# supported image types for opening are: gif, jpeg, png, wbmp, xbm
	if (!$error) {
		switch ($image_types[$original_type]) {
			case "gif":
				if (!($original_image = imagecreatefromgif($file)))
					$error.= "Error opening file.\n";
				break;
			case "jpeg":
				if (!($original_image = imagecreatefromjpeg($file)))
					$error.= "Error opening file.\n";
				break;
			case "png":
				if (!($original_image = imagecreatefrompng($file)))
					$error.= "Error opening file.\n";
				break;
            case "bmp":
                if (!($original_image = imagecreatefrombmp($file)))
                    $error.= "Error opening file.\n";
                break;
			case "wbmp":
				if (!($original_image = imagecreatefromwbmp($file)))
					$error.= "Error opening file.\n";
				break;
			case "xbm":
				if (!($original_image = imagecreatefromxbm($file)))
					$error.= "Error opening file.\n";
				break;
			default:
				$error.= "Unsupported image type.\n";
		}
	}

# if an error occured, output an error image
	if ($error) {
		header("Content-type: image/png");
		$error = substr($error,0,-1);
		$error_image = imagecreate(127,30);
		$error_background_color = imagecolorallocate($error_image,255,255,255); # white
		$error_msg_color = imagecolorallocate($error_image,255,0,0);            # red
		imagefilledrectangle($error_image,0,0,126,29,$error_background_color);
		$lines = explode("\n",$error);
		for ($i=0; $i<count($lines); $i++)
			imagestring($error_image,1,5,5+$i*(imagefontheight(1)+5),$lines[$i],$error_msg_color);
		imagepng($error_image);
		exit;
	}
# now nothing can be wrong, manipulation begins
	if (!$width && !$height && !$increase && (!$decrease || $defaultDecreaseUsed)) {
		if ($defaultWidth) $width = $defaultWidth;
		if ($defaultHeight) $height = $defaultHeight;
	}
	if ($width || $height) {
		if ($ratio) {
			if ($width && $height)
				$decrease = max($original_width/$width,$original_height/$height);
			elseif ($width)
				$decrease = $original_width/$width;
			else
				$decrease = $original_height/$height;
		} else {
            $decrease = 0;
			if ($width && $height) {
				$new_width = $width;
				$new_height = $height;
			} elseif ($width) {
				$new_width = $width;
				$new_height = $original_height;
			} else {
				$new_width = $original_width;
				$new_height = $height;
			}
		}
	} elseif ($increase)
		$decrease = 1/$increase;
	if ($decrease == 1 || (!isset($_GET['enlarge']) && $decrease<1))
		$new_image = $original_image;
	else {
		if (!$new_width && !$new_height) {
			$new_width = $original_width/$decrease;
			$new_height = $original_height/$decrease;
		}
		$new_image = imagecreatetruecolor($new_width,$new_height);
		imagecopyresized($new_image,$original_image,0,0,0,0,$new_width,$new_height,
						 $original_width,$original_height);
	}
    
# if caching enabled, save it
    if ($use_cache && !isset($_GET['nocache']) && !isset($_GET['enlarge']) && $defaultTypeUsed)
    {
        # if $filename contains path to the image, create needed directory if doesn't exist
        if (($sep = strrpos($filename,'/')) !== false)
        {
            # whether cached images are to be made
            $ok = true;
            # explode to dirs, make them gradually
            $dirs = explode('/',$filename);
            if (count($dirs)-1 > $allow_cache_dir_level)
            {
                $ok = false;
            }
            # initial directory is $path_cache (without last slash)
            $cur_dir = substr($path_cache,0,-1);
            # gradually make directories
            for ($i=0; $ok && $i<count($dirs)-1; $i++)
            {
                $cur_dir .= '/' . $dirs[$i];
                if (!file_exists($cur_dir))
                {
                    $ok &= mkdir($cur_dir);
                }
            }
        }
        
        if ($ok && file_exists($path_cache . $filename))
        {
            # if exists a thumbnail, which has wrong proportion, unlink it
            $ok &= unlink($path_cache . $filename);
        }
        
        # only if directories creation was successful and doesn't exist anymore (if ever existed)
        if ($ok)
        {
            switch ($type) {
                case "jpeg":
                    imagejpeg($new_image,$path_cache . $filename,$quality);
                    break;
                case "png":
                    imagepng($new_image, $path_cache . $filename);
                    break;
                case 'gif':
                    imagegif($new_image, $path_cache . $filename);
                    break;
                case "bmp":
                    imagebmp($new_image, $path_cache . $filename);
                    break;
                case "wbmp":
                    imagewbmp($new_image,$path_cache . $filename);
                    break;
            }
        }
    }
    
# output to browser
	if(!$save_only)
	{
	switch ($type) {
		case "jpeg":
			header("Content-type: image/jpeg");
			imagejpeg($new_image,"",$quality);
			break;
		case "png":
			header("Content-type: image/png");
			imagepng($new_image);
			break;
        case 'gif':
            header("Content-type: image/gif");
            imagegif($new_image);
            break;
        case "bmp":
            header("Content-type: image/bmp");
            imagebmp($new_image);
            break;
		case "wbmp":
			header("Content-type: image/wbmp");
			imagewbmp($new_image);
	
		break;
	}
	exit;
	}else
				
            switch ($type) {
                case "jpeg":
		    imagejpeg($new_image,$save_to,$quality);
                    break;
                case "png":
                    imagepng($new_image, $save_to);
                    break;
                case 'gif':
                    imagegif($new_image, $save_to);
                    break;
                case "bmp":
                    imagebmp($new_image, $save_to);
                    break;
                case "wbmp":
                    imagewbmp($new_image,$save_to);
                    break;
		    }
	
?>