<?php
$resulttable="";

$results = safe_query("select team,problem,count(*) as attempts, max(time) as mtime, 
sum(sc_answers.pattern like concat('%|',sc_log.answer,'|%')) as ok
 from sc_log left join sc_answers on sc_answers.id = sc_log.problem group by team,problem");

$orders[0] = new being('*,(select count(distinct problem) from sc_logcor where correct > 0 and team=sc_users.id) as solved,
(select sum(previous) from sc_logcor where team=sc_users.id and correct > 0) as totattempts,
(select sum(time) from sc_logcor where team=sc_users.id and correct > 0) as totaltime','rights<4 and school !="underground" order by solved desc, 1200*totattempts+totaltime','');

$orders[1] = new being('*,(select count(distinct problem) from sc_logcor where correct > 0 and team=sc_users.id) as solved,
(select sum(previous) from sc_logcor where team=sc_users.id and correct > 0) as totattempts,
(select sum(time) from sc_logcor where team=sc_users.id and correct > 0) as totaltime','rights<4 and school ="underground" order by solved desc, 1200*totattempts+totaltime','');

$table=$attempts=$time=$succ_attempts=array();

while($result=mysql_fetch_assoc($results))
{
	//print_r($result);
	$id=$result["team"];
	if(!isset($table[$id]))$table[$id]=array("time"=>0,"solved"=>0);
	$problem=$result["problem"];
	if($result["ok"])
	{
		$table[$id]["solved"]++;
		$table[$id]["time"]+=$result["mtime"];
		$table[$id]["c$problem"]=" class=\"gn\"";
		$solvers[$problem]++;
		$time[$problem]+=$result["mtime"];
		$succ_attempts[$problem]+=intval($result["attempts"]);
	}
	$tsolvers[$problem]++;
	$attempts[$problem]+=intval($result["attempts"]);
	$table[$id]["a$problem"]=intval($result["attempts"])-$result["ok"];
}
foreach($orders as $order){
$resulttable.=("<table><tbody><tr><th>Pořadí</th><th>Název týmu</th>
<th>Úloh<th>Čas</th><th colspan=20>Vyřešené úlohy</th></tr><tr>");
$rank=1;
while($order->fetch())
{
//print_r($order->cols);
$span = SETS;
	$row=$table[$order->cols["id"]];
	$resulttable.=("<td rowspan=$span>$rank</td>
	<td rowspan=$span>".preg_replace("/$(([^ ]{20})|(.{20}[^ ]*)).*/","$1",$order->cols["name"])."</td>
	<td  rowspan=$span>".$order->cols["solved"]."</td>
	<td  rowspan=$span>".floor($order->cols["totattempts"]*20+$order->cols["totaltime"]/60)."</td>");
	$rank++;
for($j=0;$j<SETS;$j++)	
{for($k=1;$k<=TASKS;$k++)
{
	$i=TASKS*$j+$k;
	$resulttable.=("<td".$row["c$i"].">".($row["a$i"]?$row["a$i"]:"&nbsp;")."</td>");

}$resulttable.=("</tr><tr>");
}
	
}
$resulttable.=("</tbody></table>");
}
file_put_contents("cache.txt",$resulttable);

$statstable="<table><tbody><tr><th>Příklad</th><th>Vyřešili</th><th>Zkoušeli</th><th>Průměrný čas</th><th>Pokusů (úspěšní)</th><th>Pokusů (všichni)</th></tr>";
for($problem=1;$problem<=SETS*TASKS;$problem++)
{
$tsolvers[$problem]+=0;
$solvers[$problem]+=0;
$timep=$solvers[$problem]?round(($time[$problem]/$solvers[$problem])/60):"?";
$sap=$solvers[$problem]?round($succ_attempts[$problem]/$solvers[$problem],1):"?";
$ap=$tsolvers[$problem]?round($attempts[$problem]/$tsolvers[$problem],1):"?";
$statstable.="<tr><th>$problem</th><td>$solvers[$problem]</td>
<td>$tsolvers[$problem]</td><td>$timep</td><td>$sap</td><td>$ap</td></tr>";
}
file_put_contents("statscache.txt",$statstable."</tbody></table>");
?>
