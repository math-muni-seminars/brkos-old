<?php 
define("LEGEND",
"<div class=\"legend\">Legenda: <img src=\"files/images/zadani.gif\"> = zadání,
<img src=\"files/images/reseni.gif\"> = řešení,
<img src=\"files/images/poradi.gif\"> = pořadí,
<img src=\"files/images/komentar.gif\"> = komentář,
<img src=\"files/images/povidani.gif\"> = povídání.
</div>");

define("TASKS",15);
define("SETS",3);
define("SECONDS_PER_SET",3600);
include TEXY_ADDR;

function mysql_escape($array)
{
	foreach($array as $a=>$b)
		$arrray[$a]=mysql_escape_string($b);
	return $array;
}
function date2time($date)
{
	if($date)
	{
		list($day,$month,$year)=explode(".",$date);
		return mktime(0,0,0,$month,$day,$year);
	}
}

function limit($url_param,$per_page)
{
	$page=isset($_GET[$url_param])?intval($_GET[$url_param]):1;
	return($per_page*($page-1)).",$per_page";
}

function indent($text,$n)
//{{{ Returns the $text with all the rows indented by $n
{
	$s="";
	for($i=0;$i<$n;$i++)$s.=" ";
	return $s.str_replace("\n","\n$s",$text);
}//}}}

function scandir_type($dir,$type)
{
	$dh = @opendir($dir);
		$files=array();
		while (false !== ($filename = @readdir($dh)))
		{
			if(strpos(strtolower($filename),$type)!==false)
				$files[] = $filename;
		}
	return($files);	
}

function searchfile($dir,$pat)
{
	$dh = @opendir($dir);
		$files=array();
		while (false !== ($filename = @readdir($dh)))
		{
			if(preg_match("/$pat/",$filename))
				return $dir."/".$filename;
		}
	return false; //($files);	
}

function safe_query($query)
{
	$result=@mysql_query($query);
	if(isset($_GET["debug"]))echo"$query<br>\nResult:".mysql_error().@mysql_num_rows($result)."\n<br>";
	if($result)return $result;
	else
	{
                
		$log=@file_get_contents("mysql_err.log");
		$log.="$query\n ".mysql_error()."\n".Date("Y-m-j h:i:s",Time())."\n\n";
		@file_put_contents("mysql_err.log",$log);
		return 0;
	}
}

function dbconnect()
//{{{ Connects to the DB using the constants set above
{
	$kod=SQL_ENCODING;
	if(!mysql_connect(SQL_HOST,SQL_USERNAME,SQL_PASSWORD))
	return false;
	mysql_select_db(SQL_DBNAME);
	safe_query("SET character_set_results=$kod");
	safe_query("SET character_set_connection=$kod");
	safe_query("SET character_set_client=$kod");
	return true;
}//}}}

function my_field($table,$col,$cond="1")
//{{{ Gets one field from MySQL
#table - table name
#cols - column to get
#cond - where clause of the query
{
   $res=@mysql_fetch_row(safe_query("select $col from $table where $cond limit 1"));
   return $res[0];
}//}}}

function this_url($forbid=array(),$amponly=0)
//{{{ Returns url of the current page including only the main parameter
{
	$out="";
	foreach($_GET as $key=>$value)
	{
		$value = htmlspecialchars($value);
		if(isset($forbid[$key]))$value=$forbid[$key];
		if($value!="NOT_SET")
$out=($out?$out."&".($amponly?"":"amp;"):"?")."$key=$value";
	}
	foreach($forbid as $key=>$value)
	{
		if(!isset($_GET[$key])&&($value!="NOT_SET"))
$out=($out?$out."&".($amponly?"":"amp;"):"?")."$key=$value";
	}
	return"index.php$out";
}//}}}

function output_type($x)
{

	if($_SESSION["sciuser"]->is_admin())
		return "<p>".($x?"<a href=\"".this_url(array("mode"=>"NOT_SET"))."\">HTML výstup</a>":"<a href=\"".this_url(array("mode"=>"TeX"))."\">TeX výstup</a>")."</p>";
	return "";
}


function sanitize_math($matches)
# string sanitize_math (array matches)
# odstrani vsechny mezery, kde je treba nahradi {}
# krom toho namisto $..$ vrati <img src="...">
{
    return sprintf('<img src="http://forkosh.dreamhost.com/mimetex.cgi?%s" alt="%s" class=tex>', preg_replace('/\s+/','',preg_replace('/([a-z])\s+([a-z])/', '$1{}$2', $matches[1])),$matches[1]);
}

function entex($text)
{
	$re = '/\$+([^$]+)\$+/';
	return preg_replace_callback($re, 'sanitize_math', $text);
}

function mysql_descape_string($str)
{
	return strtr($str,array("\\\""=>"\"","\\\'"=>"\'"));
}

function strlastpos($haystack,$needle)
{
	for($i=strlen($haystack)-1;$i>=0;$i--)
	{
		if($haystack[$i]==$needle){return $i;}
	}
	return false;
}
$texy = null;
function parse($pattern,$that)
{
global $texy;
        if(is_object($that))$cols=$that->cols;else $cols=$that;
	$pieces=explode("#",$pattern);
		$n=count($pieces);
		for($i=1;$i<$n;$i+=2)
		{
			$slices=explode("|",$pieces[$i]);
			$m=count($slices);
			if(isset($cols[$slices[0]]))
				$t=mysql_descape_string($cols[$slices[0]]);
			else
				$t="";
			for($j=1;$j<$m;$j++)
			{
				switch($slices[$j])
				{
				case"cut":{
				$length=$slices[++$j];
				if($length+3<strlen($t))$t=substr($t,0,$length)."...";
				};break;
				case"rmbegin":$t=substr($t,$slices[++$j]);break;
				case"cuthard":$t=substr($t,0,$slices[++$j]);break;
				case"intval":$t=intval($t);break;
				case"split":$t=substr($t,0,strpos($t,$slices[++$j]));break;
				case"dismiss":$t=str_replace($slices[++$j],"",$t);break;
				case"antispam":$t=str_replace("@","&#64;<!---->",$t);break;
                                case"outerlink":if($t){$t=substr($t,0,6)=="http:/"?$t:"http://$t";$t="<a href=\"$t\" target=\"_blank\">$t</a>";}break;
                                case"innerlink":if($t){$t=substr($t,0,6)=="http:/"?$t:"http://$t";$t="<a href=\"$t\">$t</a>";}break;
				case"date":$t=date($slices[++$j],intval($t));break;
				case"editlink":
					$t.="\n<a href=\"".this_url(array("s"=>$that->edit_page(),"edit"=>($cols["id"]),"a"=>"edit".$that->table(),"t"=>"NOT_SET"))."\">".
					   "Editace</a>\n";break;
				case"dellink":$t.="<a href=\"".this_url(array("del"=>($cols["id"]),"a"=>"delete".$that->table()))."\"".
					" onclick=\"return confirmme('".($cols["name"]?$cols["name"]:"")."')\">".
					   "Smazat</a>\n";break;
				case"isadmin":if(!(isset($_SESSION["sciuser"])&&$_SESSION["sciuser"]->is_admin()))		
					$t="";break;
				case"islogged":if(!(isset($_SESSION["sciuser"])&&$_SESSION["sciuser"]->is_logged()))		
					$t="";break;	
				case "texy":if($texy==null)$texy=new Texy; $t=$texy->process(entex($t));break;
				case "unwrap":$t=substr($t,$beg=(strpos($t,">")+1),strlastpos($t,"<")-$beg);break;//FIXME
				case "interval":$t=intval($t);$t=(date("j-n-y",$t)==date("j-n-y",$e=intval($cols[$slices[$j+1]]))?
						date($slices[$j+3],$t):date($slices[$j+2],$t)." &ndash; ".date($slices[$j+3],$e));$j+=3;break;
				case "ref":$t=preg_replace("/\[([0-9]*)\]/","[<a href=index.php?s=diskuse&u=\$1#p\$1>\$1</a>]",htmlentities($t,ENT_QUOTES,"UTF-8"));break;
				case "entities":$t=htmlentities($t,ENT_QUOTES,"UTF-8");
				case "dc":$t=(str_replace(".",",",$t));break;
				case "phantom":$dp=strpos($t,",");if(!$dp)$dp=strlen($t);
					if($dp<($ddp=$slices[++$j]))$t="\\phantom{".substr("0000",0,$ddp-$dp)."}$t";
					break;
				default:$t.=$slices[$j];
				}
			}
			$pieces[$i]=$t;
		}
		return str_replace("&35;","#",implode("",$pieces));
}


class form{
	//{{{
	var $method;
	var $params;
	var $gwb; //green wobbly bit
	var $footer; //</ul></form>
	var $labels;
	var $types;
	var $data;
	var $query;
	var $location;
	var $musts;
	var $mails;
	var $mutlipart;
	var $mysql_id;
	function form($ilabels=array(),$from=array(),$itypes=array(),$imusts=array(),
	              $action_params=array(),$method="post",$ilocation="",$iquery="",$imails=array(),
		      $imail_subject="")
	//{{{generates form
	{
		$this->data=$from;
		$this->method=$method;
		$this->params=$action_params;
		$this->musts=$imusts;
		$this->labels=$ilabels;
		$this->location=$ilocation;
		$this->query=$iquery;
		$this->mails=$imails;
		$this->types=$itypes;
		$this->mail_subject=$imail_subject;
		$this->footer="  </ul>\n</form>";
		$this->multipart=0;
		foreach($this->labels as $name=>$label)
		$this->add_field($name,$itypes[$name],$from[$name]);
		$mails=array();
	}//}}}

	function submit()
	{ 	
		$bugs="";
		foreach($this->mails as $mail=>$alias)
		{
			$bugs=mail($mail,$this->mail_subject,$this->mail_content(),
 "From: ".$this->mail_back()." \nMIME-Version: 1.0\nContent-type: text/plain; charset=\"utf-8\"")?
			"":"Mail na $mail se nepodařilo odeslat.";
		}
		if(($this->query)&&($this->query[0]!="*"))
		{
			foreach($this->musts as $key)
				if(empty($_REQUEST[$key]))$bugs.="Chybí povinný údaj: ".$this->labels[$key].".<br>";
			if(!$bugs)
				if(safe_query($this->query))
				{
					if($this->query[0]=="i")$this->mysql_id=mysql_insert_id();
					if($this->location)header("Location:".$this->location);
				}
				else $bugs.="Operace se nezdařila.".$this->query;
			return $bugs;
		}
		return substr($this->query,1);
		
	}
	
	function move_uploaded($path="#filename#")
	{
		$o=0;
		foreach($_FILES as $index=>$file)
		{
			$mask=substr($this->types[$index],5,1000);
			$args=$_POST;
			$args["filename"]=$file["name"];
			list($foo,$args["ext"])=explode(".",$file["name"]);
			$fn=parse($path,$args);
			if(preg_match("%$mask%i",$file["name"]))
			{
				$o=move_uploaded_file($file["tmp_name"],$fn);
			}
		}
		return $o;
	}

	function mail_back()
	{
		return $this->name." <".$this->data["mail"].">";
	}
	
	function mail_content()
	{
		$content="";
		foreach($this->labels as $var=>$label)
		{
			if($this->types[$var]!="submit")
			$content.=$label.":\n".$this->data[$var]."\n\n";
		}
		return $content;
	}
	
	function add_field($name,$type,$value)
	//{{{adds a field of a selected type
	{
		$label=$this->labels[$name];
		$atrib="name=\"$name\" id=\"$name\"";
		if($type=="submit")
		{
			if(isset($_REQUEST[$name]))$this->header=
				"<div class=\"bugs\">".$this->submit()."</div>".$this->header;
			$this->gwb.="   <li><input type=\"submit\" name=\"$name\" value=\"$label\" /></li>\n";
		}
		else
		{
            $class=in_array($name,$this->musts)?"class=\"fat\"":"";
            $this->gwb.=$label?"    <li><label ".$class." for=\"$name\">$label</label>":"<li>";
			if(substr($type,0,4)=="area")
			{
				$rowscols=explode("x",substr($type,4,100));
				$this->gwb.="\n<textarea cols=\"".intval($rowscols[1])."\" rows=\"".
				            intval($rowscols[0])."\" $atrib>$value</textarea></li>\n";
			}
			elseif(substr($type,0,6)=="select")
			{
				$this->gwb.="<select $atrib>\n";
				$options=explode("|",$type);
				$i=1;
				while($options[$i]||$options[$i]==="0")
				 $this->gwb.="<option value=\"".$options[$i]."\"".
				 ($value==$options[$i++]?" selected ":"")
				 .">".$options[$i++]."</option>\n";
				$this->gwb.="</select></li>\n"; 
			}
			elseif(substr($type,0,4)=="file")
			{
				$this->gwb.="    <input type=\"file\" value=\"$value\" $atrib /></li>\n";
				$this->multipart=1; 
			}
			else
			{
				if($type=="date"&&(strpos($value,".")===false)){$value=Date("j.n.Y",intval($value));$type="text";};
				$this->gwb.="    <input type=\"".($type?$type:"text")."\" value=\"$value\" $atrib />".
				"</li>\n";
			}
		}
	}//}}}

	function output()
	{
		return "<form action=\"".this_url($this->params)."\" method=\"".$this->method."\" ".($this->multipart?"enctype=\"multipart/form-data\"":"").">\n".
		              "  <ul>\n".$this->header.$this->gwb.$this->footer;
	}
}//}}}

class myrecord{
	//{{{
	var $cols;
	var $result;
	var $condition;
	var $limit;
	var $form;

	function on_delete_pars()
	{
		return array("a"=>"NOT_SET","del"=>"NOT_SET","is_confirmed"=>"NOT_SET");
	}
	function myrecord($icols,$param,$ilimit="")
	//{{{ sets the result property, for limit 1 uses fetch
	{
		$join=method_exists($this,"join")?$this->join():"";
		if($icols)$this->result=safe_query(
			"select $icols from ".$this->table()." $join where $param".($ilimit?" limit $ilimit":""));
		$this->condition=$param;
		$this->limit=$ilimit;
		if($ilimit=="1")return $this->fetch();
	}//}}}
	
	function rights($action,$user)
	{
		return true;
	}
	
	function edit_page()
	{
		return substr($this->table(),3);
	}
	
	function fetch()
	//{{{ fetches cols from result, returns 1 iff successful
	{
		if($this->cols=@mysql_fetch_assoc($this->result))
		{
			if(method_exists($this,"initialize"))$this->initialize();
			if($_SESSION["sciuser"]->is_admin() && isset($_GET["a"])&&($_GET["a"]=="delete".$this->table())
			&&($_GET["del"]==$this->cols["id"]))
			{
				safe_query("delete from ".$this->table()." where id=".$this->cols["id"]);
				header("Location:".this_url($this->on_delete_pars(),1));
			}
			return 1;
		}
		return 0;
	}//}}}

	function flush($pattern=null)
	//{{{ returns output formed from the cols using a given pattern
	{   
		return parse($this->pattern($pattern)."\n",$this);
	}//}}}
	
	function to_array($column)
	{
		$output=array();
		while($this->fetch())
		{
			$output[$this->cols[$column]]=$this->cols;
		}
		return $output;
			
	}
	
	function d()
	{
		return 5;
	}
        
	function pageurl($pattern,$p)
	{
		return this_url(array("pg"=>$p));
	}
	function listing($id,$max,$pattern=null)
	{
		if($max>1)
		{
			$sides=$this->d($pattern);
			$output="";
			if ($id>1)
				$output.="<a href=\"".$this->pageurl($pattern,$id-1)."\"><img src=\"files/images/left.gif\" alt=\"&lt;&lt; předchozí\"  style=\"vertical-align:middle\"></a>\n";
			//else $output.="<span class=\"dead\">&lt;&lt; předchozí</span>\n";
			//$output.="|";
			for($p=max($id-$sides,1);
			        $p<=min($id+$sides,$max);$p++)
				$output.=($p!=$id?"<a href=\"".$this->pageurl($pattern,$p)."\">$p</a>\n":"<B>$p </B>\n");
			//$output.="|";
			if ($id<$max)$output.="<a href=\"".$this->pageurl($pattern,$id+1)."\"><img src=\"files/images/right.gif\" alt=\"následující &gt;&gt;\" style=\"vertical-align:middle\"></a>\n";
			//else $output.="<span class=\"dead\">následující &gt;&gt;</span>\n";
			return $output;
		}
	}
	
	function navigation()
	{
		$lim=explode(",",$this->limit);
		$numres=my_field($this->table(),"count(*) as number",$this->condition);
		return "<p class=\"navigation\">\n  ".
		       $this->listing(round($lim[0]/$lim[1])+1,
		                      ceil($numres/$lim[1]))."\n</p>";


	}

	function flush_all($pattern=null,$paging=0)
	//{{{ flushes all the records it can fetch from the result
	{
		$out="";
		if($paging%2)$out.=$this->navigation();
		while($this->fetch())$out.=$this->flush($pattern);
		if($paging>1)$out.=$this->navigation();
		return $out;
	}//}}}

	//{{{form constants (declared as functions)
	function form_types()
	//{{{for the default class returns empty array
	{
		return array();
	}//}}}

	function form_labels()
	//{{{for the default class returns empty array
	{
		return array();
	}//}}}

	function form_musts()
	//{{{for the default class returns empty array
	{
		return array();
	}//}}}

	function form_action_params()
	//{{{for the default class returns empty array
	{
		return array();
	}//}}}

	function editform_location()
	//{{{for the default class returns empty string
	{
		return this_url(array("a"=>"NOT_SET","edit"=>"NOT_SET","del"=>"NOT_SET",
			"t"=>$_GET["edit"]),1);
	}//}}}
	
	function addform_location()
	//{{{for the default class returns empty string
	{
		return this_url(array("a"=>"NOT_SET","edit"=>"NOT_SET","del"=>"NOT_SET"),1);
	}//}}}

	function add_query()
	//{{{for the default class returns empty string
	{
		return "";
	}//}}}
	
	function edit_query()
	//{{{for the default class returns empty string
	{
		return "";
	}//}}}
	//}}}

	function add_form($from,$mails=array(),$subject="")
	//{{{creates and returns a form filled with the values of cols
	{
		$this->form=new form(
			$this->form_labels(),$from,$this->form_types(),
			$this->form_musts(),$this->form_action_params(),"post",
			$this->addform_location(),$q=$this->add_query(mysql_escape($from)),$mails,$subject
		            );
		return $this->form->output();
	}//}}}
	
	function edit_form()
	//{{{creates and returns a form filled with the values of cols
	{
		$this->form=new form(
			$this->form_labels(),(empty($_POST)?$this->cols:$_POST),$this->form_types(),
		                $this->form_musts(),$this->form_action_params(),"post",
		                $this->editform_location(),$this->edit_query(mysql_escape($_POST))
		            );

		return $this->form->output();
	}//}}}

}//}}}

class poll extends myrecord{
function table()
{return "sc_poll";}
function pattern($char)
{return "$char#answer#(#name#)";}
}

class question extends myrecord{
var $answers;
function table()
{return "sc_questions";}

function get_answers()
{
	$ans=new poll("*","userid=".$_SESSION["sciuser"]->cols["id"]." and ".$this->condition,"");
	$this->answers=$ans->to_array("question");
}

function pattern($f)
{
	if($f!="org")
	{	
		$value=$this->answers[$this->cols["question"]]["answer"];
		$value_top=$this->answers[100*intval($this->cols["question"])]["answer"];
		$value_speed=$this->answers[100*intval($this->cols["question"])+1]["answer"];
		$value_dif=$this->answers[100*intval($this->cols["question"])+2]["answer"];
		$value_text=$this->answers[100*intval($this->cols["question"])+3]["answer"];
		if($this->cols["format"]=="text")
			return "#content#<div><textarea name=\"poll_#poll#_#question#\" cols=50 rows=4>$value</textarea></div>";
		if(substr($this->cols["format"],0,6)=="select")
		{
			$out="<div>#content#<select name=\"poll_#poll#_#question#\">";
			$opts=explode("|",$this->cols["format"]);
			for($i=1;isset($opts[$i]);$i++)
			{
				$out.="<option value=$i ".
				($this->answers[$this->cols["question"]]["answer"]==$i?"selected":"")." >".$opts[$i]."</option>";
			}
			return "$out</select></div>";
		}
		if($this->cols["format"]=="lecture")
			return "#content#
		<div>
		Zajímavé téma
		<input type=radio name=\"poll_#poll#_#question#_top\" value=1 ".($value_top=="1"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_top\" value=2 ".($value_top=="2"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_top\" value=3 ".($value_top=="3"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_top\" value=4 ".($value_top=="4"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_top\" value=5 ".($value_top=="5"?"checked":"").">
		Nezajímavé téma
		</div>
		<div>
		Všechno jsem pochopil
		<input type=radio name=\"poll_#poll#_#question#_speed\" value=1 ".($value_speed=="1"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_speed\" value=2 ".($value_speed=="2"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_speed\" value=3 ".($value_speed=="3"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_speed\" value=4 ".($value_speed=="4"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_speed\" value=5 ".($value_speed=="5"?"checked":"").">
		Nedalo se to stíhat
		</div>
		<div>
		Dozvěděl/a jsem se spoustu věcí
		<input type=radio name=\"poll_#poll#_#question#_dif\" value=1 ".($value_dif=="1"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_dif\" value=2 ".($value_dif=="2"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_dif\" value=3 ".($value_dif=="3"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_dif\" value=4 ".($value_dif=="4"?"checked":"").">
		<input type=radio name=\"poll_#poll#_#question#_dif\" value=5 ".($value_dif=="5"?"checked":"").">
		Všechno jsem znal/a už před přednáškou
		<h5>Poznámky</h5>
		<div><textarea name=\"poll_#poll#_#question#_text\" cols=50 rows=2>$value_text</textarea></div>
		</div>";
	}
	else
	{
	$cols="*,(select name from sc_users where sc_users.id=userid) as name";
		if($this->cols["format"]=="text")
		{
		
			$answers=new poll($cols,"question=".$this->cols["question"]." and ".$this->condition,"");
			return "#content#<ol>".$answers->flush_all("\n<li>")."</ol>";
		}
		if($this->cols["format"]=="lecture")
		{
			$question=100*$this->cols["question"];
			$answers_top=new poll($cols,"question=".($question)." and ".$this->condition,"");
			$answers_speed=new poll($cols,"question=".(1+$question)." and ".$this->condition,"");
			$answers_dif=new poll($cols,"question=".(2+$question)." and ".$this->condition,"");
			$answers_text=new poll($cols,"question=".(3+$question)." and ".$this->condition,"");
			return "#content#\n<br>Téma:".$answers_top->flush_all(" ")."\n<br>Rychlost:".
			$answers_speed->flush_all(" ")."\n<br>Přínos:".$answers_dif->flush_all(" ").
			"\n<ol>".$answers_text->flush_all("\n<li>")."</ol>";
		}
		if(substr($this->cols["format"],0,6)=="select")
		{
			$answers=new poll($cols,"question=".$this->cols["question"]." and ".$this->condition,"");
			$pos=explode("|",$this->cols["format"]);
			return "#content#<p><ol>".strtr($answers->flush_all("<li>"),$pos)."</ol></p>";
			
		}
	}
}

}

class school extends myrecord{
function table()
{
return "sc_schools";
}
function pattern()
{
return "|#id#|#city#: #fullname# ";
}
}

class being extends myrecord{
	//{{{
	var $msg;
	function table()
	{return "sc_users";}
	function pattern($type)
	//{{{
	{
		switch($type)
		{
		case"participant_detailed": return "<img src=\"".(file_exists("avatars/user".$this->cols["id"].".jpg")?"avatars/user#id#.jpg":"avatars/default_face.jpg")."\" alt=\"#name#\" style=\"float:left\"><p>#name#</p>
			#school# <p>#address#</p> #email#
                      <div>#member1# #member2# #member3# #member5#</div>";
			default: 
			$img=file_exists("avatars/user".$this->cols["id"].".jpg")?	
				"<a href=avatars/big#id#.jpg class=\"thickbox\" rel=\"tymy\" title=\"#name#\"> 
<img src=\"thumb.php?width=150&amp;height=100&amp;file=avatars/user#id#.jpg\" alt=\"#name#\"  style=\"margin:bottom:1.5em\"></a>"
				:"<img src=\"avatars/default_face.jpg\" alt=\"#name#\" style=\"margin:bottom:1.5em\">";
			return "<div class=\"teamwrap\">$img</div><h4>#name#</h4>#school#
                      <ul><li>#member1#</li> <li>#member2#</li> <li>#member3#</li> <li>#member4#</li></ul>
		      <hr class=\"cleaner\">";
		}
	}//}}}
	
	function mailform()
	{
		$form=new form(
		          array("sender"=>"Odesilatel","subject"=>"Předmět",
		                "content"=>"Obsah zprávy","send"=>"Odeslat"),
		          $_POST,array("content"=>"area6x20","send"=>"submit"),array(),array("a"=>"NOT_SET"));
		return $form->output();
	}

	function is_admin()
	{
		return $this->cols["rights"]==4?1:0;
	}

	function is_tester()
	{
		return $this->cols["rights"]>2?1:0;
	}
        
	function is_logged()
	{
		return $this->cols["rights"]>1?1:0;
	}
	function handle_login()
	//{{{handles the user saved in session and sent by POST
	{
		$this->msg="";
		if(isset($_POST["login"]))
		{
		    $testlogin = new being("*","name='".mysql_escape_string($_POST["login"])."'
				and pass='".sha1($_POST["pass"])."'","1");
		    $this->cols = $testlogin->cols;
			if(!$this->cols["name"])
			{
				$this->cols=array("name"=>"","rights"=>0);
				$this->msg="Špatně zadané jméno nebo heslo.";
			}
			elseif($this->cols["pass"]!=sha1($_POST["pass"]))
			{
                                $this->cols=array("name"=>"","rights"=>0);
				$this->msg="Špatně zadané heslo.";
			}
			else header("Location: ".this_url(array(),1));	
			
		}
		elseif(!isset($this->cols))
			$this->cols=array("name"=>"","rights"=>0);
		if($_GET["a"]=="logout")
		{
			$this->cols=array("name"=>"","rights"=>0);
			header("Location: ".this_url(array("a"=>"NOT_SET"),1));
		}
	}//}}}

	function loginner()
	//{{{returns the content of loginner div
	{
		if(!$this->is_logged())
		{
			$form=new form(
		          array("login"=>"Login","pass"=>"Heslo","log"=>"Odeslat"),
		          $_POST,array("login"=>"text\" size=\"10","pass"=>"password\" size=\"10","log"=>"submit"));
			return $this->msg.$form->output()."<br>&nbsp;&nbsp;<a href=\"index.php?s=keygen\">Zapomenuté heslo?</a><br><br>";
		}
/*		else return "<p>Jste přihlášen jako ".$this->cols["name"].".
		<ul><li><a href=\"index.php?s=answers\">Zadat odpovědi</a>
		<li><a href=\"".this_url(array("a"=>"logout"))."\">Odhlásit</a></li></ul>";*/

else return "<p>Jste přihlášen jako ".$this->cols["name"].".
		<ul>
		<li><a href=\"".this_url(array("a"=>"logout"))."\">Odhlásit</a></li></ul>";
    				
	}//}}}
               
        function form_labels()
        {
                return array("name"=>"Název týmu","newpass"=>"Heslo","confpass"=>"Heslo znovu",
		"mail"=>"Kontaktní mail","address"=>"Kontaktní adresa","school"=>"Škola",
		"member1"=>"1. člen","member2"=>"2. člen","member3"=>"3. člen","member4"=>"4. člen",
		"icon"=>"Fotka týmu","send"=>"Registrovat");
        }
        
        function form_types()
        {
		return array("newpass"=>"password","confpass"=>"password","send"=>"submit","address"=>"area3x30","about"=>"area3x30",
		"icon"=>"file|.*\\.(jpg|jpeg)");
        }
        
        function form_musts()
        {
		return array("name","newpass","confpass","mail","address","school","member1","member2");
                //return array("name","first","last","mail","school","math","graduate","address");
        }
        
        function edit_query($data)
        {
		if($data["newpass"]!=$data["confpass"])
			return "*Nové heslo se neshoduje s potvrzením.";
                foreach($data as $a=>$b)$this->cols[$a]=$b;
                return "update ".($this->table())." set 
                name='".$data["name"]."',".
                ($data["newpass"]?"pass='".sha1($data["newpass"])."',":"").
                "school='".mysql_escape_string($data["school"])."',
		mail='".mysql_escape_string($data["mail"])."',
                address='".mysql_escape_string($data["address"])."',
                member1='".mysql_escape_string($data["member1"])."',
		member2='".mysql_escape_string($data["member2"])."',
                member3='".mysql_escape_string($data["member3"])."',
		member4='".mysql_escape_string($data["member4"])."'
                where id=".$this->cols["id"];
        }
	
	function add_query($data)
        {
		if($data["newpass"]!=$data["confpass"])
			return "*Nové heslo se neshoduje s potvrzením.";
                return "insert into ".($this->table())." values(0,
                '".mysql_escape_string($data["name"])."',
                '".sha1($data["newpass"])."',2,
                '".mysql_escape_string($data["school"])."',
                '".mysql_escape_string($data["mail"])."',
                '".mysql_escape_string($data["address"])."',
                '".mysql_escape_string($data["member1"])."',
		'".mysql_escape_string($data["member2"])."',
		'".mysql_escape_string($data["member3"])."',
		'".mysql_escape_string($data["member4"])."')";
        }
        function addform_location(){
		return "?s=tymy&success=1";
	}
        function editform_location()
        {
                return "";
        }
}
//}}}

class www extends myrecord{
	//{{{
	function table()
	{return "sc_www";}
	function pattern()
	{
                $img=file_exists($path="files/images/link".$this->cols["id"].".gif")?"<a href=\"http://#url#\"><img src=\"$path\" alt=\"#name#\" style=\"float:left;margin-top:1.5em\"></a>":"";
		return"$img<h4 style=\"margin-top:1.5em\"><a href=\"http://#url#\">#name#</a></h4>
		      <p>#description|texy|unwrap# (<span class=\"date\">Vloženo #date|date|j.n.Y#</span>)
			</p>#|editlink|dellink|isadmin#<hr class=cleaner>";
	}
	function form_labels()
	{
		return array("url"=>"Adresa","name"=>"Název","cat"=>"Kategorie","description"=>"Popis","send"=>"Odeslat");
	}
	
	function form_types()
	{
		return array("send"=>"submit","description"=>"area5x50",
			"cat"=>"select|1|Semináře MUNI|2|Semináře pro SŠ|3|Soutěže pro SŠ|4|Semináře ZŠ|5|MUNI|6|Různé");
	}

	function form_musts()
	{
		return array("url","name");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,".intval($data["cat"]).",'".mysql_escape_string($data["url"])."','".
		mysql_escape_string($data["name"])."','".mysql_escape_string($data["description"])."',".Time().")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table().
		" set cat=".intval($data["cat"]).", url='".$data["url"]."',name='".$data["name"]."',description='".$data["description"].
		"', date=".Time()." where id='".$this->cols["id"]."'";
	}
}//}}}

class cat extends myrecord{
	function table()
	{return "sc_cats";}
	
	function pattern($pattern="default")
	{
		switch($pattern)
		{
		case "menu": return"<h5>
			<a href=\"index.php?s=".htmlentities($_GET[s])."&amp;cat=#cats_id#\">#cats_name# (#number#)</a> 
			</h5>\n";
		case "selspec": return"|#cats_id#|#cats_name#";	
		
		default:return"<a href=\"index.php?s=shop&amp;t=#id#\">Podrobně &raquo;</a>";
		}
	}
	
};

class news extends myrecord{
	function table()
	{return "sc_news";}
	function pattern()
	{
		return"<div class=\"newstop\"><h2>#name#</h2><div class=newsdate>(#date|date|j.n.Y H:i#)</div><hr class=cleaner></div>
			<p>#content|texy|unwrap#</p>
			#|<p>|editlink|dellink|</p>|isadmin#";		
	}
	function form_labels()
	{
		return array("name"=>"Nadpis","content"=>"Obsah","send"=>"Odeslat");
	}

	function form_types()
	{
		return array("send"=>"submit","content"=>"area3x30");
	}

	function form_musts()
	{
		return array("name","content");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,'".mysql_escape_string($data["name"])
		       ."','".mysql_escape_string($data["content"])."',".Time().")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table()." set name='".mysql_escape_string($data["name"])."',
		content='".mysql_escape_string($data["content"])."',date=".Time()." where id=".$this->cols["id"];
	}
	
};

class event extends myrecord{
	function table()
	{return "sc_events";}
	function edit_page()
	{return "soustredka";}
	function editform_location()
	{return this_url(array("a"=>"NOT_SET","t"=>$this->cols["url"]),1);}
	function pattern($pattern="default")
	{
		switch($pattern)
		{
		case "preview": return"<h3>#name#</h3>
			<p>#begin|interval|end|j.n.|j.n.Y#</p>
			<p>#place#</p>
			<p>#about|texy#</p>
			<p><a href=\"index.php?s=soustredka&amp;t=#url#\">Přihláška a seznam účastníků &raquo;</a></p>
			#|editlink|dellink|isadmin#\n";
		case "menu": return "<div class=bigcol><a href=index.php?s=soustredka&amp;t=#url#>#name#</a></div> ";
		case "mailhdr": return "Přihláška na #name#";	
		case "old":return "<h2>#name#</h2>
			<p>#begin|interval|end|j.n.|j.n.Y#</p>
			<p>#place#</p>
			<p>#report|texy#</p>
			<p>#|editlink|dellink|isadmin#\n</p>";
		default:return"<h2>#name#</h2>
			<p>#begin|interval|end|j.n.|j.n.Y#</p>
			<p>#place#</p>
			<p>#|editlink|dellink|isadmin#\n</p>
			<h2>Seznam účastníků</h2>\n";
		}
	}
	function form_labels()
	{
		return array("name"=>"Název","begin"=>"Od","end"=>"Do",
			"place"=>"Místo","about"=>"Popis","report"=>"Kronika",
			"photos"=>"Fotky","users"=>"Účastníci","send"=>"Odeslat");
	}
	
	function form_types()
	{
		return array("send"=>"submit","about"=>"area20x50","report"=>"area20x50",
			"begin"=>"date","end"=>"date","users"=>"area12x50");
	}

	function form_musts()
	{
		return array("begin","end","name");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,'".beauty($data["name"])."','".
		mysql_escape_string($data["name"])."','".mysql_escape_string($data["place"]).",".date2time($data["begin"])."','".
		date2time($data["end"])."','".mysql_escape_string($data["about"])."','".
		mysql_escape_string($data["report"])."','".mysql_escape_string($data["photos"])."','".mysql_escape_string($data["users"])."')";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table().
		" set name='".$data["name"]."',
		place='".$data["place"]."', 
		begin='".date2time($data["begin"]).
		"',end='".date2time($data["end"]).
		"',about='".$data["about"].
		"', report='".$data["report"].
		"', photos='".$data["photos"].
		"', users='".$data["users"].
		"'  where id=".intval($this->cols["id"]);
	}
	
}

class booked extends myrecord{
	var $userids;
	function initialize()
	{
		if(!isset($this->userids))$this->userids=array();
		$this->userids[$this->cols["id"]]=$this->cols["accepted"];
	}
	function contains_id($id)
	{
		//print_r($this->userids);
		if(!isset($this->userids))$this->flush_all();
		return ($this->userids[$id]==2);
	}
	
	function set_event_cols($ec)
	{$this->event_cols=$ec;}
	function table()
	{return "sc_booked";}
	
	function join()
	{
		return "left join sc_events on sc_events.id=sc_booked.event 
			left join sc_users on sc_users.id=userid";
	}
	
	function pattern($pattern="default")
	{
		switch($pattern)
		{
		default: return"<h4>#first# #last# &ndash; #name#</h4>
		#school#  #pointsum# bodů  (#mail|antispam#)</p>";
		}
	}
	
}

class comment extends myrecord{
	//{{{
	var $subject;
	var $subject2;
	var $lastgal;
	
	function table()
	{return "sc_comments";}
	
	function edit_page()
	{return "gallery";}
	
	function rights()
	{
		return $_SESSION["sciuser"]->is_logged();
	}
	function comment($icols,$isubject,$ilimit,$order="date")
	{
		list($this->subject,$this->subject2)=explode("/",$isubject);
		if(isset($_GET["a"])&&($_GET["a"]=="edit".$this->table))
			$this->condition="id='".htmlentities($_GET["edit"])."'";
		else
			$this->condition=
			"subject LIKE '".$this->subject."' ".($this->subject2?"and subject2='".$this->subject2."'":"");
		$this->result=safe_query("select $icols from ".$this->table()." ".$this->join().
			" where ".$this->condition." order by $order limit $ilimit");
		$this->limit=$ilimit;
		$this->lastdir="";
		if($ilimit=="1") $this->fetch();
		return $this->cols;
	}
	
	function pattern($type)
	{
		switch($type)
		{
		case 2:return "<h3>#name# (#date|date|j.n.y#)</h3> #content|texy# #|dellink|isadmin#";break;	
		case "newcomment":"<h3>Nový komentář</h3>";break;	
		default:return "#content|cut|100#";
		}
	}
	
        function join()
        {
                return "left join sc_users on sc_users.id=sc_comments.userid";
        }
        
	function form_labels()
	{
		return array("content"=>"Komentář","send"=>"Odeslat");
	}

	function form_types()
	{
		return array("send"=>"submit","content"=>"area2x20");
	}

	function form_musts()
	{
		return array("content");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,'".$this->subject.
		       "','".$this->subject2."',".$_SESSION["sciuser"]->cols["id"].",'".mysql_escape_string($data["content"])."',".Time().")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table()."
		set content='".mysql_escape_string($data["content"])."' where id=".$this->cols["id"];
	}
		
	function out($heading)
	{ 
		if(isset($_GET["a"])&&($_GET["a"]=="edit".$this->table()))
		{return $this->edit_form();}
		$output="$heading\n";
		if(strpos($this->limit,",")!==false)$output.=$this->navigation()."\n";
		do{
			if($this->cols["content"])$output.=$this->flush(2);
		}
		while($this->fetch());
		if($this->rights("add",$_SESSION["sciuser"]))
		{	
		$output.=$this->pattern("newcomment");
		$output.=$this->add_form($_POST);
		}
		return $output;
	}
}//}}}

class blog extends myrecord{
	//{{{
	function table(){return "sc_blog";}
	function edit_page(){return "articles";}
	function on_delete_pars()
	{
		return array("a"=>"NOT_SET","del"=>"NOT_SET","t"=>"NOT_SET","is_confirmed"=>"NOT_SET");
	}
	function pattern($pattern)
	{
		switch($pattern)
		{
		case 2: return"<h2>#name#</h2>
			              #intro|texy##content|texy#
			              <div class=\"date\">Vložil #author#, #date|date|j.n.Y#</div>#|editlink|dellink|isadmin#";
		case 3: return "#intro|texy#<br />#|editlink|isadmin#";
		case "search":return "<h2><a href=\"index.php?s=articles&amp;t=#id#\">#name#</a></h2>
			              #intro|cut|300|texy#... 
				      #|editlink|dellink|isadmin#
				      <div class=\"date\">Vložil #author#, #date|date|j.n.Y#</div>";
		default:return"<h2><a href=\"index.php?s=articles&amp;t=#id#\">#name#</a></h2>
			              #intro|texy#... 
				      #|editlink|dellink|isadmin#
				      <div class=\"date\">Vložil #author#, #date|date|j.n.Y#</div>";
		}
	}
	
	function form_labels()
	{
		return array("type"=>"Typ","name"=>"Název",
		             "intro"=>"Úvod","content"=>"Obsah","send"=>"Odeslat");
	}

	function form_types()
	{
		return array("type"=>"select|misc|Zobrazit ve článcích|core|Přímý odkaz","send"=>"submit","intro"=>"area10x50","content"=>"area30x50");
	}

	function form_musts()
	{
		return array("type","name","intro");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,'".$data["type"]."','".
		$data["name"]."','".$data["intro"]."','".$data["content"]."',".Time().",".intval($_SESSION["sciuser"]->cols["id"]).")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table().
		" set type='".$data["type"]."',name='".$data["name"]."',intro='".$data["intro"]."',content=
		       '".$data["content"]."',date=".Time().",author=".intval($_SESSION["sciuser"]->cols["id"])." where id='".$this->cols["id"]."'";
	}
	

}//}}}


class gallery extends myrecord{
	//{{{
	function table(){return "sc_events";}
	function edit_page(){return "soustredka";}
	function x(){return 4;}  //columns
	function y(){return 4;}  //rows
	function d($mode){return $mode=="photo"?4:10;}  //# of fwd/back links in the navigation bar
	var $max; //# of the photos in the gallery
	var $foto_url; //url of the current photo
	var $foto_id; //id of the current photo
	var $no_pages;
	var $files;
	var $mode; //thumb or photo mode
	var $comment; //class comment; contains all the comments in photo mode and the first one in thumb.
	function url_param($type)
	{
		return $type=="thumb"?"pg":"no";
	}
	function pageurl($type,$no)
	{
		return "?s=gallery&amp;t=".$this->cols["id"]."&amp;".$this->url_param($type)."=$no#x";
	}

	function navigace_foto()
	//{{{ pannel with photopages
	{
		$output=" <p class=\"navigation\">";
		$output.=$this->listing($this->foto_id,$this->max,"photo");
		$output.=" </p>\n";
		return $output;
	}//}}}

	function navigace_nahled()
	//{{{ pannel with thumbpages
	{
		$output=" <p class=\"navigation\">";
		$output.=$this->listing($this->thumb_page,
		                        ceil($this->max/($this->x()*$this->y())),"thumb");
		$output.= "</p>\n";
		return $output;
	} //}}}

	function initialize()
	//{{{ opens directory with photos, crunches the POST params into mode and #
	{
		$this->files=scandir_type(PHOTO_DIR."/".$this->cols["url"],"jpg");
		sort($this->files);
		$this->max=count($this->files);
		$this->no_pages=ceil($this->max/$this->x()/$this->y());
		if($ph=$_GET[$this->url_param("photo")])
		{
			$this->foto_id=$ph;
			$this->thumb_page=ceil($ph/$this->x()/$this->y());
			$this->mode="photo";
		}
		elseif($ph=$_GET[$this->url_param("thumb")])
		{
			if(!($this->thumb_page=$_GET[$this->url_param("thumb")]))
				$this->thumb_page=1;
			$this->foto_id=($this->thumb_page-1)*$this->x()*$this->y()+1;
			$this->mode="thumb";
		}
		else
		{
			$this->foto_id=rand(1,$this->max);
			$this->mode="thumb";
		}
		$this->set_foto();
	}//}}}

	function set_foto($thumbsize=130)
	//{{{ sets foto_url, thumb_url and comment for the current photo
	{
		$this->foto_url=PHOTO_DIR."/".$this->cols["url"]."/".
		                $this->files[-1+$this->foto_id];
		if($this->mode=="thumb")
		{
			$this->thumb_url="thumb.php?file=".$this->foto_url."&amp;width=$thumbsize&amp;height=$thumbsize";
			$this->comment= new comment("content","photo".$this->cols["id"]."/".
			                            $this->files[-1+$this->foto_id],"1");
		}
		else
		{
			$this->comment= new comment("sc_comments.*, sc_users.name","photo".$this->cols["id"]."/".
			                            $this->files[-1+$this->foto_id],limit("pg",10));
			$this->comment->fetch();
		}
	}//}}}

	function thumb_out($target="self")
	//{{{ returns HTML code of the thumbnail
	{
		if($target=="self")$url=$this->pageurl("photo",$this->foto_id);
		else $url=$this->pageurl("thumb",1);
		$atrib="alt=\"".$this->comment->flush(1)."\" title=\"".$this->comment->flush(1)."\"";
		return "<a href=\"$url\"><img src=\"".$this->thumb_url."\" $atrib /></a>";
	}//}}}

	function out($fotosize=600,$thumbsize=130)
	//{{{ Returns the table of images or thumbs, depending on this->mode
	{
		$output.="<h2 id=\"x\">".$this->cols["name"]."</h2>";
		$output.=" <a href=\"".this_url(array("t"=>"NOT_SET","pg"=>"NOT_SET","no"=>"NOT_SET"))."\">Seznam alb</a>";
		$output.="| <a href=\"".$this->pageurl("thumb",$this->thumb_page)."\">".$this->cols["name"]."</a>";
		if($this->mode=="thumb")
		{
			$output.="<table id=\"gallery\">\n";
			for($i=1;$i<=$this->y();$i++)
			{
				$output.="\n<tr>";
				for($j=1;$j<=$this->x();$j++)
				{
					if($this->foto_id<=$this->max)
					{
						$output.="<td>".$this->thumb_out()."</td>\n";
						$this->foto_id++;
						$this->set_foto($thumbsize);
					} else {$output.="<TD>&nbsp</TD>";};
				}
				$output.="</tr>";
			}
			$output.="</table>";
			$output.=$this->navigace_nahled();
		}
		else
		{
			list($width, $height, $type, $attr) = getimagesize($this->foto_url);
			$description=strtr($this->comment->flush(1),array("\n"=>""));
			if($width>$fotosize){$nwidth=$fotosize;
				$height=round(($height*$fotosize)/$width);
				$width=$nwidth;
			
			$js="<a href=\"#\" onclick=\"popimg('$this->foto_url','$description')\" id=\"zoom\"><img src=\"files/images/lupa.gif\" alt=\"Zvětšit\"></a>";
			}
			else
				$js="";
			$atrib="alt=\"$description\" id=\"fotka\" title=\"$description".
			       "\" width=$width height=$height";
			$out="<img src=\"".$this->foto_url."\"  $atrib />";
                        $output.="<div class=\"bigphotocell\">\n";
			if($this->foto_id<$this->max)
				$output.="<a href=\"".$this->pageurl("photo",$this->foto_id+1)."\">".$out."</a>\n $js</div>";
			else
				$output.= $out."\n";

				$output.=$this->comment->out(
					$this->pattern("comments"));
			$output.=$this->navigace_foto($this->max);
		}
		return $output;
	}//}}}
      
	function pattern($pattern="self")
	{
		switch($pattern)
		{
		case"comments":return"<h1>Komentáře</h1>";
		default:return"<div class=\"bigcol\"><h3 style=\"height:3em\">#name#</a></h3>
		       <p style=\"text-align:center\">#begin|interval|end|j.n.|j.n.Y#</p>
			<p style=\"text-align:center\">#place#</p>
			<p style=\"text-align:center\"><a href=\"index.php?s=soustredka&amp;t=#url#\">O soustřeďku</a>
			".($this->cols["photos"]?"<div><a href=\"index.php?s=gallery&amp;t=#id#\">Fotky</a></div></p>\n".
			str_replace("#","&35;",$this->thumb_out($pattern)):"").
			   "#|<div>|editlink|dellink|</div>|isadmin#</div>\n";
		}
	}

	function flush_all()
	{
		$output="<div class=\"galleries\">";
		while($this->fetch())
		{
			//$i++;
			//if($i%4==1)$output.="<tr>";
			$output.=$this->flush("o");
		}
		return $output."\n</div>\n";
	}
	
	function form_labels()
	{
		return array("name"=>"Název akce","url"=>"Adresář s fotkami","send"=>"Odeslat");
	}
	
	function form_types()
	{
		return array("send"=>"submit","comment"=>"area5x20");
	}

	function form_musts()
	{
		return array("url","name");
	}

	
	function add_query($data)
	{
		return "insert into ".$this->table()." values(0,'".$data["name"]."','".
		mysql_escape_string($data["url"])."',".Time().")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table().
		" set name='".$data["name"]."',url='".mysql_escape_string($data["url"]).
		"', date=".Time()." where id='".$this->cols["id"]."'";
	}

}//}}}

class fixed extends myrecord
{
	function table()
	{
		return "sc_fixed";
	}
	
	function pattern()
	{
		return "#content|texy#
		#|editlink|isadmin#";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table().
		" set content='".$data["content"]."', date=".Time().",title='".$data["title"].
		"',description='".$data["description"].
		"', keywords='".$data["keywords"]."' where id='".$this->cols["id"]."'";
	}
	
	function form_labels()
	{
		return array("content"=>"Obsah","title"=>"Titulek","description"=>"Popis",
		             "keywords"=>"Klíčová slova","send"=>"Odeslat");
	}

	function form_types()
	{
		return array("send"=>"submit","content"=>"area30x50");
	}
        
	function editform_location()
	{
		return("index.php?s=".$this->cols["url"]);
	}
	
}
class topic extends myrecord
{
	function table()
	{return "sc_topics";}
	
	function join()
	{return "left join sc_posts on sc_posts.topic=sc_topics.id";}
	
	function pattern($pat)
	{
		switch($pat)
		{
		case "name":return "<h2>#name#</h2>";
		default:return "<div class=\"inverse\"><a href=\"index.php?s=diskuse&amp;t=#id#\">#name#</a>
		#unseen#/#number# příspěvků. <br>Poslední příspěvek: #last_name# #last_date|date|j.n.y (H:i:s)#</div>";
	}
	}
}

class post extends myrecord
{
	function table()
	{return "sc_posts";}
	
	function join()
	{return "left join sc_users on sc_posts.userid=sc_users.id";}
	
	function pattern()
	{return "<div class=\"inverse\"><span class=\"fat\"><a name=p#id#>[#id#]</a> #name|entities#</span>  #mail|isadmin# #|dellink|isadmin# 
#date|date|j.n.y (H:i:s)#</div>
	#content|ref|texy#\n";
}
	
	function form_labels()
	{
		if($_SESSION["sciuser"]->is_logged())
			return array("content"=>"","send"=>"Odeslat");
		return array("name"=>"Jméno","mail"=>"Email",
			"antispam"=>"Sem napiš křestní jméno Henryho Klevra","content"=>"","send"=>"Odeslat");
	}
	
	function form_types()
	{
		return array("content"=>"area7x70","send"=>"submit");
	}
	
	function add_query($data)
	{
		if(!$_SESSION["sciuser"]->is_logged())
		{
			if(strtolower($data["antispam"])=="henry")
			{
				safe_query("insert into sc_users (id,name,pass,mail) values
					(0,'".mysql_escape_string($data["name"])."','".Time().mysql_escape_string($data["name"])."','".mysql_escape_string($data["mail"])."')");
				$userid=mysql_insert_id();
			}
			else $userid=0;
		}
		else $userid=$_SESSION["sciuser"]->cols["id"];
		if($userid) return "insert into ".($this->table())." values (0,".
			intval($data["topic"]).",".intval($userid).",'".
			mysql_escape_string($data["content"])."',".Time().",'".$data["sign"]."')";
		return "*Špatně vyplněné antispamové políčko.";
	}
	function form_musts()
	{
		if($_SESSION["sciuser"]->is_logged())
			return array("content");
		return array("name","mail");
	}
}

class results extends myrecord
{
	function table()
	{return "sc_results";}
}


class team extends myrecord
{
	function table()
	{
		return "sc_teams";
	}
	
	function form_labels()
	{
		return array("name"=>"Název týmu","userid1"=>"První člen","userid2"=>"Druhý člen","userid3"=>"Třetí člen",
			"userid4"=>"Čtvrtý člen","contact"=>"Kontakt (mail,telefon)","send"=>"Registrovat"); 
	}
	function form_types()
	{
		$people=new being("id,first,last","first!='' and
			((select id from sc_teams where sc_users.id in (userid1,userid2,userid3,userid4)) is null) 
			and schoolid=".$_SESSION["sciuser"]->cols["schoolid"]." order by school,last","");
		$options=$people->flush_all("select");
		return array("userid1"=>"select|+| $options","userid2"=>"select|+| $options",
			"userid4"=>"select|+| $options","userid3"=>"select|+| $options",
			"contact"=>"area4x20","send"=>"submit");
	}
	function form_musts()
	{
		return array("name","userid1","userid2","userid3","userid4","contact");
	}
	function add_query($data)
	{
		return "insert into ".($this->table())." values(0,'"
		.mysql_escape_string($data["name"])."',"
		.intval($data["userid1"]).","
		.intval($data["userid2"]).","
		.intval($data["userid3"]).","
		.intval($data["userid4"]).",'"
		.mysql_escape_string($data["contact"])."')";
	}	
	function pattern()
	{
		return "<h3 id=\"#name#\">#name# (#school#)</h3>
		#person1#, #person2#, #person3#, #person4#";
	}
}

class event_comment extends comment
{
	function form_types()
	{
	return array("content"=>"area10x50","send"=>"submit");
	}
}
class keyword extends myrecord
{
	function table()
	{return "sc_dictionary left join sc_keywords on sc_dictionary.id=sc_keywords.keyword";}
	function pattern($pattern=0)
	{if($pattern=="menu")return"<div><a href=\"index.php?s=download&t=#word#\">#word#</a> (#count(*)#)</div>";
	return " <a href=\"index.php?s=download&t=#word#\">#word#</a>,";}
}
class download extends myrecord
{
	function table()
	{return "sc_download";}
	
	function add_query($data)
	{
		if($data["name"])
		{
			$table_info=mysql_fetch_assoc(safe_query("SHOW TABLE STATUS LIKE 'sc_download'"));
			$autofile=$table_info["Auto_increment"];
			$table_info=mysql_fetch_assoc(safe_query("SHOW TABLE STATUS LIKE 'sc_dictionary'"));
			$autoword=$table_info["Auto_increment"];
			$kwdb=new keyword("*","1","");
			$kwar=$kwdb->to_array("word");
			$kw=explode(",",$data["keywords"]);
			$keyword_add=$dict_add=array();
			foreach($kw as $w)
			{
				$w=trim($w);
				if(isset($kwar[$w]))
				{
					$keyword_add[]="($autofile,".$kwar[$w]["id"].")";
				}
				else
				{
					$keyword_add[]="($autofile,$autoword)";
					$dict_add[]="($autoword,'".mysql_escape_string($w)."')";
				}
			}
			if($keyword_add[0])
			{
				safe_query("insert into sc_keywords values ".implode(",",$keyword_add));
				safe_query("insert into sc_dictionary values ".implode(",",$dict_add));
			}
		}
		$file=current($_FILES);
		return "insert into sc_download values (0,'".
		$file["name"]."',".intval($data["rights"]).",'".
		mysql_escape_string($data["name"])."','".
		mysql_escape_string($data["description"])."')";
	}
	
	function pattern()
	{
		list($foo,$ext)=explode(".",$this->cols["filename"]);
		$size=round(@filesize("files/download/".$this->cols["filename"])/1024,1);
		$icon=file_exists("files/exts/$ext.gif")?$ext:"default";
		$size=$size>1024?round($size/1024,1)." MiB":"$size KiB";
		$kw=new keyword("*","sc_keywords.file=".$this->cols["id"],"");
		$keywords=$kw->flush_all();
		return "<h3>#name#</h3>
		<div style=\"float:left;padding-right:0.5em;padding-bottom:1em;\">
		  <a href=\"files/download/#filename#\">
		    <img src=\"files/exts/$icon.gif\" alt=\"Stáhnout ($ext)\" title=\"Stáhnout ($ext)\">
		  </a>
		  <div>$size</div>
		</div>  
		<p style=\"margin:0.2em;font-size:small\">".substr($keywords,0,strlen($keywords)-2)."</p>
		#description|texy#
		<hr style=\"clear:left; visibility:hidden\">";
	}
		
}

class problem extends myrecord
{
	function table()
	{
		return "sc_problems";
	}
	
	function join()
	{
		return " left join sc_users on
		sc_users.id=sc_problems.userid";
	}
	
	function pattern($pat)
	{
		if($pat=="tex")
		{
			return "\\subsection{Úloha #serie|rmbegin|2#.#problem#}

\\textit{(Opravovující #corrector#, počet řešitelů: #solvers#, průměrný počet bodů:#average|cuthard|4#.)}  

#comment#";
		}
		return "<a name=p#problem#> </a><h2>Úloha #serie|rmbegin|2#.#problem#</h2><p>
		<strong>Opravující:</strong> #corrector#
		<strong>Počet řešitelů:</strong> #solvers#
		<strong>Průměrný počet bodů:</strong> #average|cuthard|4# (#totalaverage|cuthard|4#) 
		</p>
		#comment|texy#";
	}
	
	function form_labels()
	{
		return array("comment"=>"Komentář","userid"=>"Opravující",
			"confirmed"=>"Korektura","submit"=>"Odeslat komentář");
	}
	
	function form_types()
	{
		$orgs=new being("id,name","rights>3 order by if(id=".$_SESSION["sciuser"]->cols["id"].",0,1),name","");
		$sel=$orgs->flush_all("selectorg");
		return array("comment"=>"area8x30","submit"=>"submit","userid"=>"select$sel",
			"confirmed"=>"select|0|neproběhla|1|proběhla");
	}
	
	function form_musts()
	{
		return array("comment");
	}
	
	function add_query($data)
	{
		return  "insert into ".$this->table()." values (".
		intval($_GET["t"]).",".intval($_GET["edit"]).",'".
		mysql_escape_string($data["comment"])."',".intval($data["userid"]).",".intval($data["confirmed"]).")";
	}
	
	function edit_query($data)
	{
		return "update ".$this->table()." set comment='".mysql_escape_string($data["comment"]).
		"',userid=".intval($data["userid"]).",confirmed=".intval($data["confirmed"])." where serie=".intval($_GET["t"])." and problem=".intval($_GET["edit"]);
	}
}
class answer extends myrecord
{
	function table()
	{return "sc_answers";}
	
	function pattern()
	{return "<h2>Úloha č.#id#</h2>".
//<strong>Formát výsledku:</strong> #format#
	      "<div><form method=post>
	<input name=answer><input type=submit value=ok name=ok>
	</form>
	</div>";}
	
}

class page
{
	var $site;
	var $item;
	var $subitem;
	var $action;
	var $paging;
	var $edit;
	var $mode;
	
	var $title;
	var $description;
	var $keywords;
	var $heading;
	var $menu;
	var $flashnews;
	var $main;
	var $footer;
	var $template;
	var $divs;
	var $show_fixed;
var $status;
	function page()
	{
		$this->title=$this->heading=$this->menu=
		$this->flashnews=$this->main="";
		$this->divs=Array();
		$this->scripts=Array();
		$this->parseurl();
		$this->show_fixed=!isset($this->item);
	}
	function add_title($at)
	{$this->title.=$at;}
	function add_heading($at)
	{$this->heading.=$at;}
	function add_menu($at)
	{$this->menu.=$at;}
	function add_div($id,$at)
	{$this->divs[$id].=$at;}
	function add_script($at)
	{$this->scripts[]=$at;}
	function add_main($at)
	{$this->main.=$at;}
	function add_keywords($at)
	{$this->keywords.=$at;}
	function add_description($at)
	{$this->description.=$at;}
	function set_template($temp)
	{$this->template=$temp;}
	function flush()
	{
		$scriptstr="";
		foreach($this->scripts as $t)
			$scriptstr.="<script src=\"files/$t\" type=\"text/javascript\"></script>\n";
		$origins=array("#title#","#keywords#","#description#","#title#","#footer#","#heading#","#main#","#menu#","#scripts#");
		$targets=array($this->title,$this->keywords,$this->description,$this->title,$this->footer,$this->heading,$this->main,$this->menu,$scriptstr);
		foreach($this->divs as $id=>$text)
		{
			$origins[]="#$id#";
			$targets[]=$text;
		}
		return str_replace($origins,$targets,$this->template);
	}
	function parseurl()
	{
		$this->site=isset($_GET["search"])?$_GET["search"]:(isset($_GET["s"])?$_GET["s"]:"news");
		if(isset($_GET["t"]))$this->item=$_GET["t"];
		if(isset($_GET["v"]))$this->ssitem=$_GET["v"];
		if(isset($_GET["pg"]))$this->sitem=$_GET["pg"];
		if(isset($_GET["u"]))$this->sitem=$_GET["u"];
		if(isset($_GET["pg"]))$this->paging=$_GET["pg"];
		if(isset($_GET["no"]))$this->ssitem=$_GET["no"];
		if(isset($_GET["a"]))$this->action=$_GET["a"];
		if(isset($_GET["edit"]))$this->edit=$_GET["edit"];
		if(isset($_GET["mode"]))$this->mode=$_GET["mode"];
	}
	function add_fixed($url)
	{
                $fixed=new fixed("*","url='$url' or url='_fail' order by url desc","1");
		if($this->show_fixed)
			$this->add_main($fixed->flush());
		$this->status=$fixed->cols["url"]!="_fail"?"200":"404";
                $this->add_title($fixed->cols["title"]);
		$this->add_keywords($fixed->cols["keywords"]);
		$this->add_description($fixed->cols["description"]);
	}
}

?>
