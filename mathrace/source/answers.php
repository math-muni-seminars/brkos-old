<?php
$team=intval($_SESSION["sciuser"]->cols["id"]);
if($team){
$resultsql=safe_query("select problem,count(*) as attempts, max(time) as mtime, 
sum(correct) as ok from sc_logcor  where team=$team group by problem");
$results=array();

while($r = mysql_fetch_assoc($resultsql)){
  $results[$r["problem"]]=$r;
}

if(isset($page->item))
{
$lock=mysql_fetch_assoc(safe_query("select time from sc_lock where team=$team"));
$problem=intval($page->item);
if($results[$problem]["ok"]){
	$page->add_main("<h2>Úloha č.$problem</h2>Tuto úlohu jste již správně vyřešili.");
}
elseif(Time()>$lock["time"])
{
	$form=new answer("*","id=$problem","1");
	if(isset($_POST["ok"]))
	{
		$answer=str_replace(",",".",$_POST["answer"]);
		$answer=preg_replace("/[^a-zA-Z0-9\.\/\(\)\-]/","",$answer);
		if (Time() > START_REAL+ceil($problem/TASKS)*SECONDS_PER_SET || Time()<START)
		{
		  $_SESSION["message"]="<div>Čas na zadání této odpovědi vypršel.</div>";
		}
		elseif(preg_match("%^".substr($form->cols["pattern"],1,strlen($form->cols["pattern"])-2)."$%i",$answer))
		{
			
			$_SESSION["message"]="<div>Blahopřejeme, správná odpověď!</div>";			
safe_query("insert into sc_log values(0,$team,$problem,
'$answer','".(Time()-START)."')");
		}
		else
		{
			$_SESSION["message"]="<div>Tato odpověď není správná</div>";
safe_query("insert into sc_log values(0,$team,$problem,
'$answer','".(Time()-START)."')");
			//pokud už má tým v databázi zámek, tak se vytvoří nový, jinak se updatuje starý
			safe_query("update sc_lock set time=".(Time()+40)." where team=$team");
			if(mysql_affected_rows() < 1){
				safe_query("insert into sc_lock values ($team, ".(Time()+40).")");
			}
		}
		header("location: ".this_url(array(),1));
		exit();

	}
	
$page->add_main($form->flush());

}else $page->add_main("Na další otázku smíte odpovídat až ".Date("H:i:s",$lock["time"]));
$logs = safe_query("select * from sc_log where problem = ".$page->item." and team=".intval($_SESSION["sciuser"]->cols["id"]));
	while($log = mysql_fetch_assoc($logs)){
		$min = floor($log["time"]/60);
		$sec = $log["time"]%60;
		$page->add_main("<p>$min"."m$sec"."s: $log[answer]</p>");
	}
	
}
	$page->add_main("<hr style=\"clear:both;visibility:hidden\"><table>");
	for($set=1;$set<=SETS;$set++)
	{
		$page->add_main("<tr>");
		for($task=1;$task<=TASKS;$task++)
		{
//print_r($results["$total"]);
			$total=($set-1)*TASKS+$task;
			if($results["$total"]["ok"])
			{
				$page->add_main("<td class=\"gn\">$total");
			}
			elseif(Time()>START_REAL+$set*SECONDS_PER_SET)
			{
				$page->add_main("<td style=\"background-color:silver\">$total");
			}
			else{
				
				$page->add_main("<td><a href=\"index.php?s=answers&amp;t=$total\">$total</a>");
				
			}
			if($wrong=($results["$total"]["attempts"]-($results["$total"]["ok"]?1:0)))
			{
				$page->add_main(":<span style=color:red>$wrong</span>");
			}
			$page->add_main("</td>\n");
		}
		$page->add_main("</tr>");
	}
	$page->add_main("</table>
	<div style='text-align:left; margin: 10px; padding: 10px; border: solid red'>
<strong>Formát odpovědi:</strong>
<ul>
<li>Pokud je řešením počet nějakých objektů a správná odpověď je <strong>nekonečno</strong>, napište <strong>„inf“</strong>.</li>
<li>Pokud je řešením <strong>racionální necelé číslo</strong>, napište jej jako <strong>zlomek v základním tvaru</strong> (např. 5/7, ale <strong>ne</strong> 10/14).</li>
<li>Pokud je řešením <strong>iracionální číslo</strong>, napište jej v <strong>desetinném zápisu</strong> s tečkou <strong>zaokrouhleno na 5 desetinných míst</strong> (např. 5.55579846… jako 5.55580).</li>
</ul>
</strong>
</div>
");
}
else
	$page->add_main("Pravděpodobně Vám vypršelo přihlášení. Přihlašte se a pak můžete zadávat odpovědi.");


?>
