<?php
require "source/rss.php";
require "config/config.php";
require "source/class.php";
Define("WEB_NAME","BRKOS :: diskuse");
Define("ABSOLUTE_ROOT_URI","http://math.muni.cz/~brkos/");
Define("RSS_DESCRIPTION","Diskusní fórum brněnského korespondenčního semináře.");
Define("EMAIL_ADMIN","kondr@lesnimoudrost.cz");


dbconnect();
Header("Content-Type: application/rss+xml; encoding=utf-8");

$Prispevky=@mysql_query("select br_posts.*, br_topics.name as topic_name, br_users.nick as username 
	from br_posts 
	left join br_topics on br_topics.id=br_posts.topic
	left join br_users on br_users.id=br_posts.userid
	order by id desc limit 30");
$RSS = new RSS;
$RSS->set_description(RSS_DESCRIPTION);
while ($Prispevek=@mysql_fetch_assoc($Prispevky)) {
	$Item= new RSS_Item;
	$Item->set_title(strtr($Prispevek["topic_name"]." - ".$Prispevek["username"],array("&"=>"a")));
	$Item->set_guid("http://bart.math.muni.cz/~brkos/index.php?s=diskuse&amp;t=$Prispevek[topic]#p$Prispevek[id]");
	$Item->set_link("http://bart.math.muni.cz/~brkos/index.php?s=diskuse&amp;t=$Prispevek[topic]#p$Prispevek[id]");
	$Item->set_description(strtr($Prispevek["content"],array("&"=>"&amp;","<"=>"&lt;",">"=>"&gt;")));
	$Item->set_category(strtr($Prispevek["topic_name"],array("&"=>"&amp;","<"=>"&lt;",">"=>"&gt;")));
	$Item->set_author($Prispevek["username"]);
	$Item->set_pub_date(Date("r",$Prispevek["date"]));
	$Items[]=$Item;
}
foreach ($Items as $Item)
{
	$RSS->add_item($Item);
}
echo $RSS->generate();

?>

