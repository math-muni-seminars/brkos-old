<?
ini_set("display_errors","On");
ini_set("error_reporting","E_ALL &! E_NOTICE");
function sanitize_math($matches)
# string sanitize_math (array matches)
# odstrani vsechny mezery, kde je treba nahradi {}
# krom toho namisto $..$ vrati <img src="...">
{
	return sprintf('<img src="http://forkosh.dreamhost.com/mimetex.cgi?%s" alt="%s" class=tex>', preg_replace('/([a-z])\s+([a-z])/', '$1{}$2', $matches[1]),$matches[1]);
}

function entex($text)
{
	$re = '/\$+([^$]+)\$+/';
	return preg_replace_callback($re, 'sanitize_math', $text);
}
function deitemize($list){
$items=explode("\\item",$list[1]);
//array_map($items,trim);
return implode("\n*",$items);
}
function detabular($table){
$t = preg_replace("/^.*\\hline/sU","|-----------------",$table[1]);
return preg_replace("/\\\\\\\\\s*.hline\s*/","\n|",strtr($t,array("&"=>"|","\n"=>"")));

}
function detheorem($s) {
$counters = array();
$names = array("reseni"=>"Řešení","uloha"=>"Úloha","pr"=>"Příklad","re"=>"Řešení");
$result = "";
while($s){
$cut = strpos($s,"\\begin");
if($cut ===false){
echo "no theorem";
$result.=$s;
$s="";
}else{
$cut2 = strpos($s,"}",$cut+6);

$thm = substr($s, $cut+7, $cut2-$cut-7);
echo $thm;
if(!isset($counters[$thm]))$counters[$thm]=1;
$result.=substr($s,0,$cut)."\n> **$names[$thm] $counters[$thm]**";
$counters[$thm]++;
$cut3=strpos($s,'\end{'.$thm.'}',$cut2);
echo $cut2."XY".$cut3;
$content = substr($s,$cut2+1,$cut3-$cut2-1);
$result.= "\n> ".preg_replace("/[\r\n\t]/"," ",$content);
$s =  substr($s,$cut3+5+strlen($thm));
}
}
return $result;
}
function tex2texy($s){
$s = strtr($s,array("measuredangle"=>"angle","\vert" => "|","\\newpage"=>"","\\noindent" => "","\begin{table}"=>"","\end{table}"=>"","\\rm"=>"","\begin{uloha}~% u"=>"==Ú"));
$s = preg_replace("/\\\\url\{(.*)\}/U"," $1 ",$s);
$s = preg_replace("/\\\\section.?\{(.*)\}/","===$1===",$s);
$s = preg_replace("/\\\\subsection.?\{(.*)\}/","==$1==",$s);
$s = preg_replace("/\\\\textbf\{(.*)\}/U"," **$1** ",$s);
$s = preg_replace("/\\\\uv\{(.*)\}/U"," \"$1\" ",$s);
$s = preg_replace("/\\\$\\\$(.*)\\\$\\\$/","\n\n.<>\n\\\$$1\\\$\n\n",$s);
$s = preg_replace("/\\\\begin\{align.?\}(.*)\\\\end\{align.?\}/sU", "\n\n .<>\n \$\begin{eqnarray}$1\end{eqnarray}\$\n", $s);
$s = preg_replace_callback("/\\\\begin\{itemize\}(.*)\\\\end\{itemize\}/sU", 'deitemize', $s);
$s = preg_replace_callback("/\\\\begin\{tabular\}(.*)\\\\end\{tabular\}/sU", 'detabular', $s);

//$s = detheorem($s);
return $s;
}

//foreach($_POST as $i=>$j)$_POST[$i]=stripslashes($j);
require "texy/texy.php";
$t = new Texy();
$r=tex2texy($_POST["source"]);
echo "<html><head><title>Tex2Texy!</title><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"></head><body>
<div style=float:left><form method=\"post\"><textarea cols=50 rows=35 name=\"source\">$_POST[source]</textarea>
<input type=submit value=go></form></div><textarea cols=50 rows=35>$r</textarea>".$t->process(entex($r))."</body></html>";
?>